package com.boncenger.nguberjek.app;

/**
 * Created by adikurniawan on 10/21/17.
 */

public interface ResponseCallback{
    int doAction(String response, int httpCode);
}

