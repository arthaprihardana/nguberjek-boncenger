package com.boncenger.nguberjek.app;

import com.boncenger.nguberjek.model.AddressModel;

/**
 * Created by sragen on 8/9/2016.
 */
public class AppConstants {
    public static final int requestTimeOut=40000;

    public static final String APIHost="http://159.203.109.20:8080/api/v3/";
    public static final String SOCKET_SERVER_URL = "http://159.203.109.20:8080";

//    public static final String APIHost="http://192.168.43.173:8080/api/v3/";
//    public static final String SOCKET_SERVER_URL = "http://192.168.43.173:8080";


    public static final boolean isRelease=false;
    public static String gcm_id="";

    public static final String APIVersion =APIHost+"info";
    public static final String APILogin =APIHost+"lgn/boncenger";
    public static final String APIRegis =APIHost+"reg/boncenger";
    public static final String APIEditProfile =APIHost+"ns/boncenger";
    public static final String APIForgotPass =APIHost+"lupaPassword/boncenger";
    public static final String APILogout =APIHost+"ns/lgo/boncenger";
    public static final String APIRiderNear =APIHost+"ws/radius/driver";
    public static final String APIHistory =APIHost+"ns/history";
    public static final String APIFare =APIHost+"ns/fare";
    public static final String APIRequestOrder =APIHost+"ws/requestOrder";
    public static final String APIDetailOrder =APIHost+"ns/orderDetail";
    public static final String APIUpdateStatus =APIHost+"ns/updateStatusOrder";
    public static final String APIUpdateStatusFood =APIHost+"ns/updateFoodOrder";
    public static final String APISendRating =APIHost+"api/v1/ratingOrder";
    public static final String APIRiderFavorit =APIHost+"ns/favorit/driver";
    public static final String APIRuteFavorit =APIHost+"ns/favorit/perjalanan";
    public static final String APiCancelRequest = APIHost+"api/v1/cancelOrder";
    public static final String APIChatList = APIHost+"ws/listChat";
    public static final String APIChatSend = APIHost+"ws/sendChat";
    public static final String APIUpload = APIHost+"ns/upload/";


    public static AddressModel addressModel;


    // before versioning //
    /*public static final String APIUploadImage =APIHost+"uploadImage";
    public static final String APILogin =APIHost+"loginboncenger";
    public static final String APIRegis =APIHost+"boncenger/registrasi";
    public static final String APIEditProfile =APIHost+"api/v1/boncenger";
    public static final String APIForgotPass =APIHost+"lupapassword/boncenger";
    public static final String APILogout =APIHost+"api/v1/logoutboncenger";
    public static final String APIRiderNear =APIHost+"api/v1/geo/radius";
    public static final String APIHistory =APIHost+"api/v1/history";
    public static final String APIFare =APIHost+"api/v1/fare";
    public static final String APIRequestOrder =APIHost+"api/v1/requestOrder";
    public static final String APIDetailOrder =APIHost+"api/v1/orderDetail";
    public static final String APIUpdateStatus =APIHost+"api/v1/updateStatusOrder";
    public static final String APISendRating =APIHost+"api/v1/ratingOrder";
    public static final String APIRiderFavorit =APIHost+"api/v1/favorit/rider";
    public static final String APIRuteFavorit =APIHost+"api/v1/favorit/rute";
    public static final String APiCancelRequest = APIHost+"api/v1/cancelOrder";
    public static final String APIChatList = APIHost+"api/v1/listChat";
    public static final String APIChatSend = APIHost+"api/v1/sendChat";*/

}


