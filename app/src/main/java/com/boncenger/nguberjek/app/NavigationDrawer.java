package com.boncenger.nguberjek.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.boncenger.nguberjek.activity.BantuanActivity;
import com.boncenger.nguberjek.activity.BookingActivity;
import com.boncenger.nguberjek.activity.FavoritActivity;
import com.boncenger.nguberjek.activity.HadiahActivity;
import com.boncenger.nguberjek.activity.RiwayatActivity;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.SessionManager;

import java.util.HashMap;

/**
 * Created by sragen on 8/12/2016.
 */
public class NavigationDrawer extends BaseActivity implements ListView.OnItemClickListener {
    Activity activity;
    Context context;
    ListView listDrawer;
    DrawerLayout mDrawerLayout;

    SessionManager session;
    HashMap<String, String> user;

    public NavigationDrawer(Activity activity, ListView listDrawer, DrawerLayout mDrawerLayout) {
        this.activity = activity;
        this.context=activity;
        this.listDrawer = listDrawer;
        this.mDrawerLayout = mDrawerLayout;

        session = new SessionManager(activity);
        user = session.getUserDetails();

        listDrawer.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView parent, View view, int position, long id) {
        selectItem(position);
        //Toast.makeText(activity,""+position+"",Toast.LENGTH_SHORT).show();
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        //Fragment fragment = new PlanetFragment();
        Bundle args = new Bundle();
        // args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);

        switch (position) {
            case 1:
                Intent intent = new Intent(activity, RiwayatActivity.class);
                Bundle b=new Bundle();
                intent.putExtras(b);
                activity.startActivity(intent);
                mDrawerLayout.closeDrawers();
                break;
            case 2:
                Intent intent2 = new Intent(activity, FavoritActivity.class);
                Bundle b2=new Bundle();
                intent2.putExtras(b2);
                activity.startActivity(intent2);
                mDrawerLayout.closeDrawers();
                break;
            /*case 3:
                Intent intent3 = new Intent(activity, HadiahActivity.class);
                Bundle b3=new Bundle();
                intent3.putExtras(b3);
                activity.startActivity(intent3);
                mDrawerLayout.closeDrawers();
                break;*/
            case 3:

                Intent intent4 = new Intent(activity, BantuanActivity.class);
                Bundle b4=new Bundle();
                intent4.putExtras(b4);
                activity.startActivity(intent4);
                mDrawerLayout.closeDrawers();

                break;
            case 4:

                Intent intent5 = new Intent(activity, BookingActivity.class);
                Bundle b5=new Bundle();
                b5.putString("status","1");
                b5.putString("url","https://docs.google.com/forms/d/e/1FAIpQLSe3-GMV03bSatKI-XgvLGEUDascRxR2biyCD1Wi1ZB_NM2AzQ/viewform");
                intent5.putExtras(b5);
                activity.startActivity(intent5);
                mDrawerLayout.closeDrawers();

                break;

            case 6:

                break;
            case 7:

                break;

            case 8:


                break;
        }

    }




}
