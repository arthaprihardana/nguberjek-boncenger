package com.boncenger.nguberjek.app;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import com.boncenger.nguberjek.BuildConfig;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by adikurniawan on 10/21/17.
 */

public class API {
    public static  String APIHost="http://159.203.109.20:8080/api/v3/ns/";
//    public static  String APIHost="http://192.168.43.173:8080/api/v3/ns/";
    public static String my_token="";
    public static String API_KEY_GOOGLE= "AIzaSyDnTce5NZQrxVHh6r7hLkAdeTF9VU8Adz4";
    public static HashMap<String,String> user;


    public static void getCategoryFood(Context context,String keyword, ResponseCallback onSuccess, ErrorCallback onError){
        String url;
        url =APIHost+"kategoriMenu";
        if(keyword.length()>0){
            url+="?search="+keyword;
        }


        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void getResto(Context context,String keyword, ResponseCallback onSuccess, ErrorCallback onError){
        String url;
        url =APIHost+"resto";
        if(keyword.length()>0){
            url+="?search="+keyword;
        }


        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void getMenu(Context context,String restoId, ResponseCallback onSuccess, ErrorCallback onError){
        String url;
        url =APIHost+"groupmenu?namaResto="+restoId;


        RequestManager.get(context, url, onSuccess, onError);
    }


    public static void setProducts(Context context,String sku, ResponseCallback onSuccess, ErrorCallback onError){
        String url;
        url =APIHost+"resto";

        Map<String, String> params = new HashMap<>();
        params.put("sku", sku);
        RequestManager.post(context, url, params, onSuccess, onError);
    }

    public static void getGeoLocation(Context context, double lat, double lng, ResponseCallback onSuccess, ErrorCallback onError){
        String url = "https://maps.googleapis.com/maps/api/geocode/json";
        url += "?latlng=" +lat +","+lng;
        url += "&key=" + API_KEY_GOOGLE;

        RequestManager.get(context, url, onSuccess, onError);
    }

    public static void getFare(Context context, String distancee_value, ResponseCallback onSuccess, ErrorCallback onError){
        String url = APIHost+"fare";
        url += "?jarak=" +distancee_value;
        url += "&role=nguberjek" ;

        RequestManager.get(context, url, onSuccess, onError);
    }


    public static class RequestManager{
        private final static OkHttpClient httpClient = new OkHttpClient();
        private static String stringifyRequestBody(Request request) {
            try {
                okio.Buffer buffer = new okio.Buffer();
                request.newBuilder().build().body().writeTo(buffer);

                return buffer.readUtf8();
            } catch (IOException e) {

            }

            return null;
        }

        private static Request getRequest(String url,RequestBody requestBody){
            Headers.Builder httpHeadersBuilder = new Headers.Builder();
            String token="";
            String ime="";

            httpHeadersBuilder.add("X-API-Client", "Nguberjek");
            httpHeadersBuilder.add("Device-ID", ime);
            httpHeadersBuilder.add("Accept", "application/json");
            if(!token.equals("")){
                httpHeadersBuilder.add("Authorization",token);
            }


            Headers httpHeaders = httpHeadersBuilder.build();
            Request.Builder requestBuilder = new Request.Builder()
                    .url(url)
                    .headers(httpHeaders);


            if (requestBody != null) {
                requestBuilder.post(requestBody);
            }

            Request request = requestBuilder.build();

            String requestBodyString = null;
            if (requestBody != null) requestBodyString = stringifyRequestBody(request);

            if (BuildConfig.DEBUG) {
                Log.d("API", "API URL: " + url);
                Log.d("API", "API Headers Sent: " + httpHeaders.toString());
                if (requestBody != null)
                    Log.d("API", "API POST : " + (requestBodyString != null ? requestBodyString : "NULL"));
            }
            return request;
        }


        private static void createHttpRequest(Context context,Request request,
                                              final ResponseCallback onSuccess, final ErrorCallback onError) {

          /*  httpClient.setCookieHandler(new CookieManager(
                    new PersistentCookieStore(context),
                    CookiePolicy.ACCEPT_ALL));
*/

            httpClient.setConnectTimeout(60, TimeUnit.SECONDS);
            httpClient.setReadTimeout(60, TimeUnit.SECONDS);

            httpClient.newCall(request).enqueue(new Callback() {
                Handler mainHandler = new Handler(Looper.getMainLooper());

                @Override
                public void onFailure(Request request, IOException e) {
                    if (BuildConfig.DEBUG) e.printStackTrace();


                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (onError != null) {
                                onError.doAction();
                            }
                        }
                    });
                }

                @Override
                public void onResponse(final Response response) throws IOException {
                    final String responseBody = response.body().string();
                    final int responseCode = response.code();

                    if (BuildConfig.DEBUG) {
                        Log.d("API", "API Response Code: " + responseCode);
                        Log.d("API", "API Response: " + responseBody);
                    }


                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (onSuccess != null) {
                                onSuccess.doAction(responseBody, responseCode);
                            }
                        }
                    });
                }
            });
        }

        public static void get(Context context,final String url,
                               final ResponseCallback onSuccess, final ErrorCallback onError) {

            Request request = getRequest(url, null);
            createHttpRequest(context,request, onSuccess, onError);
        }

        public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        public static void post_json(Context context, final String url, String json,
                                     final ResponseCallback onSuccess, final ErrorCallback onError) {

            Log.d("API", "API Media type : " + JSON);
            RequestBody body = RequestBody.create(JSON, json.getBytes());
            Request request = getRequest(url, body);

            createHttpRequest(context, request, onSuccess, onError);
        }

        public static void post(Context context,final String url, final Map<String, String> parameters,
                                final ResponseCallback onSuccess, final ErrorCallback onError) {

            FormEncodingBuilder formEncodingBuilder = new FormEncodingBuilder();

            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                if (entry.getKey() != null && entry.getValue() != null) {
                    formEncodingBuilder.add(entry.getKey(), entry.getValue());
                }
            }

            RequestBody requestBody = formEncodingBuilder.build();
            Request request = getRequest(url, requestBody);

            createHttpRequest(context,request, onSuccess, onError);
        }

        /*public static String getIMEI(){
            String imei="";
            try {
                Context ctx = LatifaApp.getAppContext();

                String deviceId = "";
                deviceId= Settings.Secure.getString(ctx.getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                if(!deviceId.equals("")) imei=deviceId;
                else imei="12345678";

            } catch (Throwable e) {
                e.printStackTrace();
            }
            return imei;
        }

        public static String getToken(){
            String token="";
            Context ctx=LatifaApp.getAppContext();
            session=new SessionManager(ctx.getApplicationContext());
            if(session.isLoggedIn()) {
                user = session.getUserDetails();
                token = user.get(SessionManager.KEY_MEMBER_TOKEN);
            }else{
                token=API.my_token;
            }

            return token;
        }*/


    }

}
