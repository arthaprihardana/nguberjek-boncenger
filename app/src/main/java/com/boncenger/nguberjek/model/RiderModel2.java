package com.boncenger.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by adikurniawan on 9/11/16.
 */
public class RiderModel2 implements Serializable{

    public String rider_id;
    public String rider_name;
    public String rider_nopol;
    public String rider_phone;
    public String rider_kategori;
    public String rider_photo;

    public RiderModel2(JSONObject data)throws JSONException {
        this.rider_id=data.getString("_id");
        this.rider_name=data.getString("nama");
        this.rider_nopol=data.getString("noPolisi");
        this.rider_phone=data.getString("noTelp");
        this.rider_kategori=data.getString("kategori");
        this.rider_photo=data.getString("photo");
        //this.rider_photo="http://128.199.201.12:8085/images/UserPhoto-oII3SERI8U4xyBomFZZu.jpeg";
    }
}
