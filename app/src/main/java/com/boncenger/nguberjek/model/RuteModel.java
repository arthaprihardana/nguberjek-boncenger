package com.boncenger.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by adikurniawan on 9/11/16.
 */
public class RuteModel implements Serializable {
    public String orderId;
    public String lokasiAwal;
    public String lokasiAkhir;
    public String kategori;
    public String jarak;
    public double awalLat;
    public double awalLng;
    public double tujuanLat;
    public double tujuanLng;


    public RuteModel(JSONObject data) throws JSONException{
        this.orderId=data.getString("orderId");
        this.lokasiAwal=data.getString("lokasi");
        this.awalLat=data.getDouble("latitudeLA");
        this.awalLng=data.getDouble("longitudeLA");


        this.lokasiAkhir=data.getString("tujuan");
        this.tujuanLat=data.getDouble("latitudeLT");
        this.tujuanLng=data.getDouble("longitudeLT");

        this.kategori=data.getString("kategori");
        this.jarak=data.getString("jarak");
    }
}
