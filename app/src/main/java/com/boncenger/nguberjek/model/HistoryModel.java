package com.boncenger.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sragen on 8/28/2016.
 */
public class HistoryModel implements Serializable {

    public String order_id;
    public String order_time;
    public String order_kategori;
    public String lokasi_awal;
    public String lokasi_tujuan;

    public HistoryModel(JSONObject data)throws JSONException{
        this.order_id=data.getString("orderId");
        this.order_time=data.getString("tanggal");
        this.order_kategori=data.getString("kategori");
        this.lokasi_awal=data.getString("lokasiAwal");
        this.lokasi_tujuan=data.getString("lokasiTujuan");
    }
}
