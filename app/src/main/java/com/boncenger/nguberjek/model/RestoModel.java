package com.boncenger.nguberjek.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by adikurniawan on 10/19/17.
 */

public class RestoModel implements Serializable {
    public String id="";
    public String name="";
    public String image="";
    public int imageDrawable=0;
    public String address="";
    public double lat=0;
    public double lng=0;
    public String openingHours="";
    public String status="";
    public String phone="";

    public RestoModel(){
        this.id="123";
        this.name="Mister Lie";
        this.image="https://s3-ap-southeast-1.amazonaws.com/sg-awan-s3.lakupon.com/cdn/600/dl-3971-201602241127344807_600.jpg";
        this.address="Jl. KH Syahdan Binus Palmerah, Jakarta";
        this.lat=-6.2005072;
        this.lng=106.7863495;
        this.openingHours="08:00 - 22:00";
        this.status="BUKA";
    }
    public RestoModel(JSONObject data)throws JSONException{
        this.id=data.getString("_id");
        this.name=data.getString("namaResto");
        this.image=data.getString("foto");
        this.address=data.getString("alamat");
        this.openingHours=data.getString("jamBuka");
        this.status=data.getString("show");
        JSONObject cor=data.getJSONObject("koordinat");
        this.lat=cor.getDouble("latitude");
        this.lng=cor.getDouble("longitude");
    }
}
