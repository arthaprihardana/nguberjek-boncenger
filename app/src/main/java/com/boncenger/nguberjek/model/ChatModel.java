package com.boncenger.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by adikurniawan on 10/8/16.
 */
public class ChatModel implements Serializable {
    public boolean isBoncenger;
    public String message;
    public String date;

    public ChatModel (JSONObject data) throws JSONException{
        this.isBoncenger= data.getBoolean("boncenger");
        this.message =data.getString("msg");
        this.date = data.getString("date");
    }

    public ChatModel (String message){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String currentDateandTime = sdf.format(new Date());

        this.isBoncenger=true;
        this.message=message;
        this.date=currentDateandTime;
    }
}
