package com.boncenger.nguberjek.model;

import com.boncenger.nguberjek.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by adikurniawan on 10/17/17.
 */

public class CategoryModel implements Serializable {
    public String id="";
    public String name="";
    public String image="";
    public int imageDrawable=0;
    public ArrayList<MenuModel> menuList=new ArrayList<MenuModel>();

    public CategoryModel(){
        this.id="";
        this.imageDrawable= R.drawable.desert;
    }
    public CategoryModel(JSONObject data)throws JSONException{
        this.id=data.getString("_id");
        this.name=data.getString("namaKategori");
        this.image=data.getString("image");
    }
    public CategoryModel(JSONObject data,boolean isMenu)throws JSONException{
        this.name=data.getString("kategori");
        JSONArray dataMenu=data.getJSONArray("data");
        for(int i=0; i<dataMenu.length();i++){
            JSONObject menu=dataMenu.getJSONObject(i);
            this.menuList.add(new MenuModel(menu));
        }
    }
}
