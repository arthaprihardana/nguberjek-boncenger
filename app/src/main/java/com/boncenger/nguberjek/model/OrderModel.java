package com.boncenger.nguberjek.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sragen on 9/8/2016.
 */
public class OrderModel implements Serializable {
    public String _id;
    public String order_id;
    public String order_location_awal;
    public String order_location_tujuan;
    public String order_price;
    public String order_distance;
    public String order_duration;
    public double order_latitude_awal;
    public double order_longitude_awal;
    public double order_latitude_tujuan;
    public double order_longitude_tujuan;
    public String order_service;
    public String rider_id="";
    public String rider_name;
    public String rider_photo;
    public String rider_nopol;
    public String rider_phone;
    public int rider_rating;
    public String order_price_food="";
    public String order_price_delivery="";
    public String order_resto_id;

    public String updated_at;
    public String created_at;
    public String kategori;
    public String status;
    public String show;
    public String setasfavorit;


    public OrderModel(){

    }

    public OrderModel(JSONObject data)throws JSONException{
        this.order_id=data.getString("orderId");
        this.order_location_awal=data.getString("lokasiAwal");
        this.order_location_tujuan=data.getString("lokasiTujuan");
        this.order_price=data.getString("harga");
        this.order_distance=data.getString("jarak");
        this.order_duration=data.getString("waktu");
        this.order_latitude_awal=data.getDouble("latitudeLA");
        this.order_longitude_awal=data.getDouble("longitudeLA");
        this.order_latitude_tujuan=data.getDouble("latitudeLT");
        this.order_longitude_tujuan=data.getDouble("longitudeLT");

        JSONObject dataRider=data.getJSONObject("driver");
        if(!dataRider.isNull("_id"))
            this.rider_id=dataRider.getString("_id");
        else this.rider_id="";


        this.rider_name=dataRider.getString("nama");
        this.rider_phone=dataRider.getString("noTelp");
        this.rider_nopol=dataRider.getString("noPolisi");
        this.rider_photo=dataRider.getString("photo");
        if(!dataRider.isNull("rating"))
            this.rider_rating=dataRider.getInt("rating");
        else this.rider_rating=5;

    }

    public OrderModel(JSONObject data, boolean isFood) throws JSONException {
        _id = data.getString("_id");
        if(!data.isNull("updated_at"))updated_at = data.getString("updated_at");
        if(!data.isNull("created_at"))created_at = data.getString("created_at");
        if(!data.isNull("kategori"))kategori = data.getString("kategori");
        status = data.getString("status");
        this.order_duration = data.getString("waktu");
        this.order_distance = data.getString("jarak");

        if(!data.isNull("totalHarga"))order_price = data.getString("totalHarga");

        this.order_id = "food_"+data.getString("orderId");
        if(!data.isNull("show")) show = data.getString("show");
        if(!data.isNull("setasfavorit")) setasfavorit = data.getString("setasfavorit");

        JSONObject dataResto=data.getJSONObject("resto");
        this.order_location_awal = dataResto.getString("alamat");
        JSONArray cor_=dataResto.getJSONArray("koordinat");
        this.order_latitude_awal = Double.parseDouble(cor_.get(1).toString());
        this.order_longitude_awal = Double.parseDouble(cor_.get(0).toString());


        this.order_location_tujuan = data.getString("alamatPengantaran");
        JSONArray cor=data.getJSONArray("koordinatPengantaran");
        this.order_latitude_tujuan=Double.parseDouble(cor.get(1).toString());
        this.order_longitude_tujuan=Double.parseDouble(cor.get(0).toString());

        /*if(!data.isNull("boncenger")) {
            JSONObject boncengerId = data.getJSONObject("boncenger");
            nama = boncengerId.getString("nama");
            noTelp = boncengerId.getString("noTelp");
        }*/

        if(!data.isNull("rider")) {
            JSONObject boncengerId = data.getJSONObject("rider");
            this.rider_id = boncengerId.getString("_id");
            this.rider_name = boncengerId.getString("nama");
            this.rider_nopol = boncengerId.getString("noTelp");
            this.rider_photo = boncengerId.getString("photo");
            this.rider_nopol = boncengerId.getString("noPolisi");
            if(!boncengerId.isNull("rating"))
                this.rider_rating=boncengerId.getInt("rating");
            else this.rider_rating=5;
        }
    }

}
