package com.boncenger.nguberjek.model;

/**
 * Created by sragen on 8/9/2016.
 */
public class Session {
    private String member_id;
    private String member_username;
    private String member_firstname;
    private String member_lastname;
    private String member_dob;
    private String member_phone;
    private String member_handphone;
    private String member_post_code;
    private String member_email;
    private String member_password;
    private String member_token;
    private String member_okTalent;
    private String member_gender;
    private String member_address;
    private String member_salary;
    private String member_smoker;
    private String member_city;
    private String member_area;
    private String member_city_name;
    private String member_area_name;
    private String member_foto;
    private String sender_id;


    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getMember_username() {
        return member_username;
    }

    public void setMember_username(String member_username) {
        this.member_username = member_username;
    }

    public String getMember_handphone() {
        return member_handphone;
    }

    public void setMember_handphone(String member_handphone) {
        this.member_handphone = member_handphone;
    }

    public String getMember_post_code() {
        return member_post_code;
    }

    public void setMember_post_code(String member_post_code) {
        this.member_post_code = member_post_code;
    }

    public String getMember_city_name() {
        return member_city_name;
    }

    public void setMember_city_name(String member_city_name) {
        this.member_city_name = member_city_name;
    }

    public String getMember_area_name() {
        return member_area_name;
    }

    public void setMember_area_name(String member_area_name) {
        this.member_area_name = member_area_name;
    }

    public String getMember_area() {
        return member_area;
    }

    public void setMember_area(String member_area) {
        this.member_area = member_area;
    }

    public String getMember_city() {
        return member_city;
    }

    public void setMember_city(String member_city) {
        this.member_city = member_city;
    }

    public String getMember_foto() {
        return member_foto;
    }

    public void setMember_foto(String member_foto) {
        this.member_foto = member_foto;
    }

    public String getMember_okTalent() {
        return member_okTalent;
    }

    public void setMember_okTalent(String member_okTalent) {
        this.member_okTalent = member_okTalent;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMember_password() {
        return member_password;
    }

    public void setMember_password(String member_password) {
        this.member_password = member_password;
    }

    public String getMember_firstname() {
        return member_firstname;
    }

    public void setMember_firstname(String member_firstname) {
        this.member_firstname = member_firstname;
    }

    public String getMember_lastname() {
        return member_lastname;
    }

    public void setMember_lastname(String member_lastname) {
        this.member_lastname = member_lastname;
    }

    public String getMember_dob() {
        return member_dob;
    }

    public void setMember_dob(String member_dob) {
        this.member_dob = member_dob;
    }

    public String getMember_phone() {
        return member_phone;
    }

    public void setMember_phone(String member_phone) {
        this.member_phone = member_phone;
    }

    public String getMember_email() {
        return member_email;
    }

    public void setMember_email(String member_email) {
        this.member_email = member_email;
    }

    public String getMember_token() {
        return member_token;
    }

    public void setMember_token(String member_token) {
        this.member_token = member_token;
    }

    public String getMember_gender() {
        return member_gender;
    }

    public void setMember_gender(String member_gender) {
        this.member_gender = member_gender;
    }

    public String getMember_address() {
        return member_address;
    }

    public void setMember_address(String member_address) {
        this.member_address = member_address;
    }

    public String getMember_salary() {
        return member_salary;
    }

    public void setMember_salary(String member_salary) {
        this.member_salary = member_salary;
    }

    public String getMember_smoker() {
        return member_smoker;
    }

    public void setMember_smoker(String member_smoker) {
        this.member_smoker = member_smoker;
    }
}


