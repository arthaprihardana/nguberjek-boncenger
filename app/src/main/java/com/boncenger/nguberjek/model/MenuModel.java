package com.boncenger.nguberjek.model;

import android.view.Menu;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by adikurniawan on 10/20/17.
 */

public class MenuModel implements Serializable {
    public String id="";
    public String name="";
    public String image="";
    public int imageDrawable=0;
    public int price=0;
    public int qty=0;
    public String note="";

    public MenuModel(){
        this.id="123";
        this.name="Chicken Cheese Katsu";
        this.image="https://assets-pergikuliner.com/YoLol8-fDIg-m95Fn_g5G3GIPWQ=/385x290/smart/https://assets-pergikuliner.com/uploads/image/picture/398600/picture-1480907588.jpg";
        this.price=26500;
    }
    public MenuModel(JSONObject data)throws JSONException{
        this.id=data.getString("_id");
        this.name=data.getString("namaMenu");
        this.image=data.getString("foto");
        this.price=data.getInt("harga");
    }
}
