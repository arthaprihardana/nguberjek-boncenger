package com.boncenger.nguberjek.model;

import java.io.Serializable;

/**
 * Created by adikurniawan on 22/11/17.
 */

public class AddressModel implements Serializable {
    public double my_lat=0;
    public double my_lng=0;
    public String my_address_name="";
    public String note="";

    public double resto_lat=0;
    public double resto_lng=0;
    public String resto_address_name="";

    public String distance="";
    public String distanceLabel="";
    public String duration="";
    public String price="";

}
