package com.boncenger.nguberjek.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sragen on 8/28/2016.
 */
public class RiderModel implements Serializable {
    public String rider_id;
    public String rider_name;
    public double rider_lat;
    public double rider_lng;
    public String rider_role;
    public String rider_nopol;
    public String rider_kendaraan;
    public String rider_merk;
    public String rider_photo;


    public RiderModel(){

    }

    public RiderModel(JSONObject data)throws JSONException{
        this.rider_id=data.getString("_id");
        this.rider_name=data.getString("nama");
        this.rider_lat=data.getDouble("latitude");
        this.rider_lng=data.getDouble("longitude");
        this.rider_role=data.getString("role");
        this.rider_nopol=data.getString("noPolisi");
        this.rider_kendaraan=data.getString("typeKendaraan");
        this.rider_merk=data.getString("merkKendaraan");
        this.rider_photo=data.getString("photo");
    }
}
