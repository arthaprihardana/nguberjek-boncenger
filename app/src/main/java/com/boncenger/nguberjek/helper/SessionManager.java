package com.boncenger.nguberjek.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.boncenger.nguberjek.MainActivity;
import com.boncenger.nguberjek.model.Session;

import java.util.HashMap;
import java.util.List;

/**
 * Created by sragen on 8/9/2016.
 */
public class SessionManager {
    List<Session> sessionList;
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences prefFirstApp;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    SharedPreferences.Editor editorFirstApp;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "PaxCustomer";
    private static final String PREF_NAME2 = "PaxCustomer2";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_MEMBER_ID = "0";
    public static final String KEY_MEMBER_FIRSTNAME = "firstname";
    public static final String KEY_MEMBER_LASTNAME = "lastname";
    public static final String KEY_MEMBER_USERNAME = "username";
    public static final String KEY_MEMBER_EMAIL = "email";
    public static final String KEY_MEMBER_GENDER = "gender";
    public static final String KEY_MEMBER_PASSWORD = "password";
    public static final String KEY_MEMBER_DOB = "dob";
    public static final String KEY_MEMBER_PHONE = "phone";
    public static final String KEY_MEMBER_TOKEN = "token";
    public static final String KEY_MEMBER_IMAGE = "foto";
    public static final String KEY_FIRST_APP="";
    public static final String KEY_MEMBER_CITY="city";
    public static final String KEY_MEMBER_CITY_NAME="city_name";
    public static final String KEY_MEMBER_ADDRESS="area";
    public static final String KEY_MEMBER_AREA="address";
    public static final String KEY_MEMBER_AREA_NAME="area_name";
    public static final String KEY_MEMBER_HANDPHONE="handphone";
    public static final String KEY_MEMBER_POSTCODE="postcode";
    public static final String KEY_VEHICLE_ID_SELECTED="policy_id";
    public static final String KEY_SENDER_ID="sender_id";



    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        prefFirstApp=_context.getSharedPreferences(PREF_NAME2, PRIVATE_MODE);
        editor = pref.edit();
        editorFirstApp=prefFirstApp.edit();
    }
    /**
     * Create login session
     * */
    public void createLoginSession(List<Session> sessionList){
        this.sessionList=sessionList;
        Session m=sessionList.get(0);

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_MEMBER_ID, m.getMember_id() );
        editor.putString(KEY_MEMBER_FIRSTNAME, m.getMember_firstname());
        editor.putString(KEY_MEMBER_LASTNAME, m.getMember_lastname());
        editor.putString(KEY_MEMBER_USERNAME, m.getMember_username());
        editor.putString(KEY_MEMBER_EMAIL, m.getMember_email() );
        editor.putString(KEY_MEMBER_PASSWORD, m.getMember_password() );
        editor.putString(KEY_MEMBER_PHONE, m.getMember_phone());
        editor.putString(KEY_MEMBER_GENDER, m.getMember_gender());
        editor.putString(KEY_MEMBER_DOB, m.getMember_dob());
        editor.putString(KEY_MEMBER_TOKEN, m.getMember_token());
        editor.putString(KEY_MEMBER_IMAGE,m.getMember_foto());
        editor.putString(KEY_MEMBER_CITY,m.getMember_city());
        editor.putString(KEY_MEMBER_AREA,m.getMember_area());
        editor.putString(KEY_MEMBER_CITY_NAME,m.getMember_city_name());
        editor.putString(KEY_MEMBER_AREA_NAME,m.getMember_area_name());
        editor.putString(KEY_MEMBER_HANDPHONE,m.getMember_handphone());
        editor.putString(KEY_MEMBER_POSTCODE,m.getMember_post_code());
        editor.putString(KEY_MEMBER_ADDRESS,m.getMember_address());
        editor.putString(KEY_VEHICLE_ID_SELECTED,"0");
        editor.putString(KEY_SENDER_ID,m.getSender_id());


        // commit changes
        editor.commit();
    }

    public void setVehicle(String vehicle_id){
        editor.putString(KEY_VEHICLE_ID_SELECTED,vehicle_id);
    }



    public  void setNotFirstApp(boolean  first_app){
        // Storing value as TRUE
        editorFirstApp.putBoolean(KEY_FIRST_APP, first_app);

        // commit changes
        editorFirstApp.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public int checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, MainActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
            return 0;
        }else return 1;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_MEMBER_ID, pref.getString(KEY_MEMBER_ID, null));
        user.put(KEY_MEMBER_EMAIL, pref.getString(KEY_MEMBER_EMAIL, null));
        user.put(KEY_MEMBER_PASSWORD, pref.getString(KEY_MEMBER_PASSWORD, null));
        user.put(KEY_MEMBER_FIRSTNAME, pref.getString(KEY_MEMBER_FIRSTNAME, null));
        user.put(KEY_MEMBER_LASTNAME, pref.getString(KEY_MEMBER_LASTNAME, null));
        user.put(KEY_MEMBER_USERNAME, pref.getString(KEY_MEMBER_USERNAME, null));
        user.put(KEY_MEMBER_PHONE, pref.getString(KEY_MEMBER_PHONE, null));
        user.put(KEY_MEMBER_GENDER, pref.getString(KEY_MEMBER_GENDER, null));
        user.put(KEY_MEMBER_DOB, pref.getString(KEY_MEMBER_DOB, null));
        user.put(KEY_MEMBER_TOKEN, pref.getString(KEY_MEMBER_TOKEN, null));
        user.put(KEY_MEMBER_IMAGE, pref.getString(KEY_MEMBER_IMAGE,null));
        user.put(KEY_MEMBER_CITY, pref.getString(KEY_MEMBER_CITY,null));
        user.put(KEY_MEMBER_AREA, pref.getString(KEY_MEMBER_AREA,null));
        user.put(KEY_MEMBER_ADDRESS, pref.getString(KEY_MEMBER_ADDRESS,null));
        user.put(KEY_MEMBER_CITY_NAME, pref.getString(KEY_MEMBER_CITY_NAME,null));
        user.put(KEY_MEMBER_AREA_NAME, pref.getString(KEY_MEMBER_AREA_NAME,null));
        user.put(KEY_MEMBER_POSTCODE, pref.getString(KEY_MEMBER_POSTCODE,null));
        user.put(KEY_MEMBER_HANDPHONE, pref.getString(KEY_MEMBER_HANDPHONE,null));
        user.put(KEY_VEHICLE_ID_SELECTED, pref.getString(KEY_VEHICLE_ID_SELECTED,"0"));
        user.put(KEY_SENDER_ID, pref.getString(KEY_SENDER_ID,null));
        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();


        // After logout redirect user to Loing Activity
        /*Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);*/
    }

    public void refreshData(){
        editor.clear();
        editor.commit();
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
    // Get FirstApp State
    public boolean isFirstApp(){
        return prefFirstApp.getBoolean(KEY_FIRST_APP, true);
    }
    public void clearFirstApp(){
        editorFirstApp.clear();
        editorFirstApp.commit();
    }
}


