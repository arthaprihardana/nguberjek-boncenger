package com.boncenger.nguberjek.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.HomeAcitivity;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.model.Session;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.socket.emitter.Emitter;

/**
 * Created by sragen on 8/9/2016.
 */
public class BaseActivity extends AppCompatActivity {
    private static Map<Class<? extends Activity>, Activity> launched = new HashMap<Class<? extends Activity>, Activity>();
    protected FrameLayout rootView;
    public static Context context;
    protected Activity mActivity;
    protected int viewHeight;
    protected int viewWidth;
    public LogManager log=new LogManager();


    public static boolean isActive = false;


    public SessionManager session;
    public HashMap<String,String> user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Activity activity = launched.get(this.getClass());
        if (activity != null)
            activity.finish();

        session=new SessionManager(this);
        user=session.getUserDetails();

        launched.put(this.getClass(), this);
        super.onCreate(savedInstanceState);
        context = this;
        mActivity=this;
        ViewUtils.GetInstance().setResolution(this);
        viewHeight = ViewUtils.GetInstance().getHeight();
        viewWidth = ViewUtils.GetInstance().getWidth();

        ActivityManager.getInstance().onCreate(this);
    }

    public static Context getContext(){
        return context;
    }

    @Override
    protected void onDestroy() {
        Activity activity = launched.get(this.getClass());
        if (activity == this)
            launched.remove(this.getClass());

        ActivityManager.getInstance().onDestroy(this);
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        resettingView();
        isActive = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive = false;
    }
    protected void resettingView()
    {
        ViewUtils.GetInstance().setResolution(this);
        viewHeight = ViewUtils.GetInstance().getHeight();
        viewWidth = ViewUtils.GetInstance().getWidth();
    }


    //check network connection
    public static boolean isOnline() {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//        return (networkInfo != null && networkInfo.isConnected());
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public boolean checkVersionCode(int versionCodeServer){
        boolean mustUpdate=false;
        int version=0;
        try{
            Context ctx = getApplicationContext();
            PackageInfo pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
            version=pInfo.versionCode;

            if(version<versionCodeServer){
                mustUpdate=true;
            }else{
                mustUpdate=false;
            }
        }catch (Throwable throwable){
            Log.e("Error versionCode",throwable.toString());
            version=0;
            mustUpdate=false;
        }
        return mustUpdate;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        resettingView();
    }



    public void setHideKeyboardOnTouch(View view) {

        //Set up touch listener for non-text box views to hide keyboard.
        if(!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(mActivity);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setHideKeyboardOnTouch(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //finish();
    }

    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    ///// DIALOGS /////

    public void dialog_message(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }


    // do login for get token //
    public void getLogin(){
        final String url= AppConstants.APILogin;
        String tag_json_obj="Login";

        final ProgressDialog dialog= ProgressDialog.show(context, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    String TOKEN=response.getString("token");

                    if(status.equals("1")){
                        JSONObject data=response.getJSONObject("dt");


                        //Toast.makeText(getApplicationContext(),"Auto get token",Toast.LENGTH_SHORT).show();
                        createSession(data,TOKEN);
                        startActivity(new Intent(context, HomeAcitivity.class));
                        finish();
                        ActivityManager.getInstance().finishAll();
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please check your internet connection !" ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", user.get(SessionManager.KEY_MEMBER_USERNAME));
                params.put("password", user.get(SessionManager.KEY_MEMBER_PASSWORD));
                params.put("senderId", user.get(SessionManager.KEY_SENDER_ID));
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void createSession(JSONObject data,String token)throws JSONException{
        String id=data.getString("_id");
        String username=data.getString("username");
        String nama=data.getString("nama");
        String email=data.getString("email");
        String phone=data.getString("noTelp");
        String gender=data.getString("jenisKelamin");
        String image=data.getString("photo");

        Session sessionModel = new Session();
        sessionModel.setMember_id(id);
        sessionModel.setMember_username(username);
        sessionModel.setMember_password(user.get(SessionManager.KEY_MEMBER_PASSWORD));
        sessionModel.setMember_firstname(nama);
        sessionModel.setMember_email(email);
        sessionModel.setMember_phone(phone);
        sessionModel.setMember_gender(gender);
        sessionModel.setMember_token(token);
        sessionModel.setMember_foto(image);
        sessionModel.setSender_id(user.get(SessionManager.KEY_SENDER_ID));
        ArrayList<Session> sessionList=new ArrayList<Session>();
        sessionList.add(sessionModel);

        session.logoutUser();
        session.createLoginSession(sessionList);
    }

    //////////////////////////////////////// SOCKET APP //////////////////////////////////////////////
    public Boolean isConnected = true;
    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!isConnected) {
                        //Toast.makeText(getApplicationContext(), R.string.connect, Toast.LENGTH_LONG).show();
                        Log.d("socket",getString(R.string.connect));
                        isConnected = true;
                    }
                }
            });
        }
    };

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    //Toast.makeText(getApplicationContext(), R.string.disconnect, Toast.LENGTH_LONG).show();
                    Log.d("socket",getString(R.string.disconnect));

                }
            });
        }
    };

    public Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Toast.makeText(getApplicationContext(), R.string.error_connect, Toast.LENGTH_LONG).show();
                    Log.d("socket",getString(R.string.error_connect));

                }
            });
        }
    };


}
