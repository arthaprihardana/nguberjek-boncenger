package com.boncenger.nguberjek.helper;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.boncenger.nguberjek.activity.HomeAcitivity;

/**
 * Created by sragen on 8/12/2016.
 */
public class TouchableWrapper extends FrameLayout {

    public TouchableWrapper(Context context) {
        super(context);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                HomeAcitivity.mMapIsTouched = true;
                break;

            case MotionEvent.ACTION_UP:
                HomeAcitivity.mMapIsTouched = false;
                break;
        }
        return super.dispatchTouchEvent(event);
    }
}