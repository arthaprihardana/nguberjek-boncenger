package com.boncenger.nguberjek.helper;

import android.app.Activity;
import android.content.Context;

import com.boncenger.nguberjek.activity.LoginActivity;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by sragen on 8/9/2016.
 */
public class ActivityManager  {
    private final static ActivityManager instance;

    private final ArrayList<Activity> activities;

    static {
        instance = new ActivityManager();
    }

    public static ActivityManager getInstance() {
        return instance;
    }
    private ActivityManager() {
        activities = new ArrayList<Activity>();
    }
    private void rebuildStack() {
        Iterator<Activity> iterator = activities.iterator();
        while (iterator.hasNext())
            if (iterator.next().isFinishing())
                iterator.remove();
    }


    public void clearStack(boolean finishRoot) {
        LoginActivity root = null;
        rebuildStack();
        for (Activity activity : activities) {
            if (!finishRoot && root == null && activity instanceof LoginActivity)
                root = (LoginActivity) activity;
            else
                activity.finish();
        }
        rebuildStack();
    }
    public void finishAll() {
        rebuildStack();
        for (Activity activity : activities) {
            activity.finish();
        }
        rebuildStack();
    }
    public boolean hasLoginActivity(Context context) {
        rebuildStack();
        for (Activity activity : activities)
            if (activity instanceof LoginActivity)
                return true;
        return false;
    }


    public void onCreate(Activity activity) {
        activities.add(activity);
        rebuildStack();
    }
    public void onDestroy(Activity activity) {
        activities.remove(activity);
    }

}


