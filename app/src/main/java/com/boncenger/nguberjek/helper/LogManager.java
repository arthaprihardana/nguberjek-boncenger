package com.boncenger.nguberjek.helper;

import android.util.Log;

import com.boncenger.nguberjek.app.AppConstants;

/**
 * Created by sragen on 8/9/2016.
 */
public class LogManager {

    public void logD(String message){
        if(!AppConstants.isRelease){
            Log.d(getClass().getSimpleName(),message);
        }
    }
    public void logI(String tag,String message){
        if(!AppConstants.isRelease){
            Log.d(tag,message);
        }
    }
    public void logW(String tag,String message){
        if(!AppConstants.isRelease){
            Log.d(tag,message);
        }
    }
    public void logE(String tag,String message){
        if(!AppConstants.isRelease){
            Log.d(tag,message);
        }
    }
}
