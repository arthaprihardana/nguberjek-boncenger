package com.boncenger.nguberjek.helper;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by sragen on 9/3/2016.
 */
public class ConvertRupiah {
    public String ConvertRupiah(String value){
        String sallary = "";
        if (value.equals("")) {
            sallary = "0";
        } else {
            sallary = value;
        }
        NumberFormat rupiahFormat = NumberFormat.getInstance(Locale.GERMANY);
        String rupiah = rupiahFormat.format(Double.parseDouble(sallary));
        return rupiah;
    }
}
