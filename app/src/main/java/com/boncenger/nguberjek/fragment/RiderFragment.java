package com.boncenger.nguberjek.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.HomeAcitivity;
import com.boncenger.nguberjek.adapter.RiderAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.LogManager;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.RiderModel2;
import com.boncenger.nguberjek.model.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sragen on 8/19/2016.
 */
public class RiderFragment extends Fragment {

    RecyclerView recyclerView;
    ProgressBar progressBar;

    private SessionManager session;
    private HashMap<String,String> user;

    private LogManager log=new LogManager();

    private ArrayList<RiderModel2> riderList=new ArrayList<RiderModel2>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_rider, container, false);

        session=new SessionManager(getActivity());
        user=session.getUserDetails();

        setContent(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState){
        getData();
    }

    public void setContent(View rootView){
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv);
        progressBar=(ProgressBar) rootView.findViewById(R.id.progressBar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void setData(){
        RiderAdapter adapter = new RiderAdapter(getActivity(),riderList);
        recyclerView.setAdapter(adapter);
    }

    private void getData(){
        final String url= AppConstants.APIRiderFavorit+"?boncengerId="+user.get(SessionManager.KEY_MEMBER_ID);
        String tag_json_obj="history";

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        JSONArray dataArray=response.getJSONArray("dt");
                        for (int i=0;i<dataArray.length();i++){
                            JSONObject data=dataArray.getJSONObject(i);
                            riderList.add(new RiderModel2(data));
                        }
                        setData();
                    }else{
                        dialog_message(message);
                    }
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } catch (JSONException e) {

                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                goLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }




    public void dialog_message(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    private void goLogin(){
        final String url= AppConstants.APILogin;
        String tag_json_obj="Login";

        final ProgressDialog dialog= ProgressDialog.show(getActivity(), "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    String TOKEN=response.getString("token");

                    if(status.equals("1")){
                        JSONObject data=response.getJSONObject("dt");


                        Toast.makeText(getActivity(),"Auto get Token ",Toast.LENGTH_SHORT).show();
                        createSession(data,TOKEN);
                        startActivity(new Intent(getActivity(), HomeAcitivity.class));
                        getActivity().finish();
                        ActivityManager.getInstance().finishAll();
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(),
                            "Please check your internet connection !" ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", user.get(SessionManager.KEY_MEMBER_USERNAME));
                params.put("password", user.get(SessionManager.KEY_MEMBER_PASSWORD));
                params.put("senderId", user.get(SessionManager.KEY_SENDER_ID));
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void createSession(JSONObject data,String TOKEN)throws JSONException{
        String id=data.getString("_id");
        String username=data.getString("username");
        String nama=data.getString("nama");
        String email=data.getString("email");
        String phone=data.getString("noTelp");
        String gender=data.getString("jenisKelamin");
        String image=data.getString("photo");

        Session sessionModel = new Session();
        sessionModel.setMember_id(id);
        sessionModel.setMember_username(username);
        sessionModel.setMember_password(user.get(SessionManager.KEY_MEMBER_PASSWORD));
        sessionModel.setMember_firstname(nama);
        sessionModel.setMember_email(email);
        sessionModel.setMember_phone(phone);
        sessionModel.setMember_gender(gender);
        sessionModel.setMember_token(TOKEN);
        sessionModel.setMember_foto(image);
        sessionModel.setSender_id(user.get(SessionManager.KEY_SENDER_ID));
        ArrayList<Session>sessionList=new ArrayList<Session>();
        sessionList.add(sessionModel);

        session.logoutUser();
        session.createLoginSession(sessionList);
    }

}
