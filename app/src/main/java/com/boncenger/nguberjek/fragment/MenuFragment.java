package com.boncenger.nguberjek.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.HomeAcitivity;
import com.boncenger.nguberjek.adapter.MenuRestoAdapter;
import com.boncenger.nguberjek.adapter.RiderAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.LogManager;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.MenuModel;
import com.boncenger.nguberjek.model.RiderModel2;
import com.boncenger.nguberjek.model.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by adikurniawan on 10/20/17.
 */

public class MenuFragment extends Fragment {

    RecyclerView recyclerView;
    ProgressBar progressBar;



    private ArrayList<MenuModel> menuList=new ArrayList<MenuModel>();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_menu_resto, container, false);


        setContent(rootView);

        return rootView;
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState){

        Bundle b=getArguments();
        menuList=(ArrayList<MenuModel>)b.getSerializable("data_menu");

        setData();
    }

    public void setContent(View rootView){
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rv);
        progressBar=(ProgressBar) rootView.findViewById(R.id.progressBar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void setData(){
        MenuRestoAdapter adapter = new MenuRestoAdapter(getActivity(),menuList);
        recyclerView.setAdapter(adapter);
    }



    public void dialog_message(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        builder.setTitle(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }


}
