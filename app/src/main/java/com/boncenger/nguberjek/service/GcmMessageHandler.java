package com.boncenger.nguberjek.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.boncenger.nguberjek.activity.ChatActivity;
import com.google.android.gms.gcm.GcmListenerService;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.HomeAcitivity;
import com.boncenger.nguberjek.activity.ReviewActivity;
import com.boncenger.nguberjek.activity.TrackingActivity;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;

import java.io.IOException;

/**
 * Created by sragen on 8/28/2016.
 */
public class GcmMessageHandler  extends GcmListenerService {
    /*
    List Status GCM :
                 (0) Info Permintaan Order (order request)
                 (1) Info rider sudah ditemukan (order diterima)
                 (2) Info kepada rider bahwa rider terpilih untuk menjalankan order (rider menjemput boncenger)
                 (3) Informasi status perjalanan sedang berlangsung
                 (4) Informasi status perjalanan telah selesai
                 (5) Informasi terjadi masalah pada saat perjalanan berlangsung (harga dipotong 50%)
                 (6) Informasi boncenger membatalkan order
                 (7) Informasi rider membatalkan order
                 (8) Info kepada rider bahwa order telah didapatkan oleh rider yang lain
                 (9) Status info tidak ada driver disekitar penumpang
                 (10) Informasi yang dikirim oleh admin
            */

    public static final int MESSAGE_NOTIFICATION_ID = 435345;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String title = data.getString("title");
        String message = data.getString("message");
        String order_id = data.getString("orderId");
        String status=data.getString("status");
        Log.d("pushnotif",data.toString());

        playSound();
        goToActivity(order_id,status,message);

        /*if(BaseActivity.isActive) {
            goToActivity(order_id,status,message);
        }else{
            createNotification(title, message, order_id,status);
        }*/

        /*if(status.equals("14")){
            createNotification(title, message, order_id,status);
        }else{
            goToActivity(order_id,status,message);
        }*/
    }




    private void goToActivity(String order_id,String status,String message){
        Log.d("pushnotif","Go Activity");
        Bundle b=new Bundle();
        Intent i;
        if(status != null && status.equals("1")){
            i=new Intent(this,TrackingActivity.class);
            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",false);
            b.putString("message",message);
        }else if(status != null && status.equals("2")){
            i=new Intent(this,TrackingActivity.class);
            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",false);
            b.putString("message",message);
        }else if(status != null && status.equals("3")){
            i=new Intent(this, TrackingActivity.class);
            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",true);
            b.putString("message",message);
        }else if(status != null && status.equals("4")){
            i=new Intent(this, ReviewActivity.class);
            b.putString("order_id",order_id);
            b.putString("message",message);
        }else if(status!= null & status.equals("14")){
            i=new Intent(this, ChatActivity.class);
            b.putString("order_id",order_id);
            b.putString("message",message);
        }else{
            i=new Intent(this, HomeAcitivity.class);
            b.putString("message",message);
        }


        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtras(b);
        startActivity(i);
        if(status!= null & !status.equals("14")){
            ActivityManager.getInstance().finishAll();
        }

    }


    // Creates notification based on title and body received
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void createNotification(String title, String message,String order_id,String status) {

        Notification.Builder mBuilder =
                new Notification.Builder(this)
                        .setSmallIcon(R.mipmap.nguberjek)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(message);

        if(Build.VERSION.SDK_INT> Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            mBuilder
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(message));
        }


        Intent push = new Intent();
        Bundle b=new Bundle();
        if(status.equals("1")) {
            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",false);
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, TrackingActivity.class);

        } else if(status.equals("2")) {
            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",false);
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, TrackingActivity.class);

        } else if(status.equals("3")){

            b.putString("order_id",order_id);
            b.putBoolean("dalam_perjalanan",true);
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, TrackingActivity.class);

        }else if(status.equals("4")){

            b.putString("order_id",order_id);
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, ReviewActivity.class);

        } else if(status.equals("14")){

            b.putString("order_id",order_id);
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, ChatActivity.class);

        }else{
            b.putString("message",message);
            push.putExtras(b);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            push.setClass(this, HomeAcitivity.class);
        }

        PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0,
                push, PendingIntent.FLAG_CANCEL_CURRENT);
        mBuilder
                .setContentText(message)
                .setFullScreenIntent(fullScreenPendingIntent, true);

        mBuilder.setContentIntent(fullScreenPendingIntent);
        mBuilder.setAutoCancel(true);

        if(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP)
        {
            mBuilder.setCategory(Notification.CATEGORY_MESSAGE);
        }


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(23, mBuilder.build());
        //mNotificationManager.notify(Integer.parseInt(status), mBuilder.build());
    }

    public void playSound(){
        Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        MediaPlayer mediaPlayer = new MediaPlayer();

        try {
            mediaPlayer.setDataSource(this, defaultRingtoneUri);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer mp)
                {
                    mp.release();
                }
            });
            mediaPlayer.start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
