package com.boncenger.nguberjek.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.boncenger.nguberjek.R;

/**
 * Created by sragen on 9/8/2016.
 */
@SuppressLint("DefaultLocale")
public class DialogCancelOrder extends Dialog implements View.OnClickListener {

    TextView tv1, tv2, tv3, tv4;
    ListenerDialog listenerDialog;

    public DialogCancelOrder(Context context, ListenerDialog listener){
        super(context, R.style.TransparantDialogTheme);
        this.listenerDialog=listener;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_cancel_order);

        tv1=(TextView)findViewById(R.id.tv1);
        tv2=(TextView)findViewById(R.id.tv2);
        tv3=(TextView)findViewById(R.id.tv3);
        tv4=(TextView)findViewById(R.id.tv4);
        tv1.setOnClickListener(this);
        tv2.setOnClickListener(this);
        tv3.setOnClickListener(this);
        tv4.setOnClickListener(this);

    }

    public interface ListenerDialog{
        void onSelected(boolean isi,String value);
    }

    @Override
    public void onClick(View v) {
        if(v==tv1){
            dismiss();
            listenerDialog.onSelected(true,tv1.getText().toString());
        }else if(v==tv2){
            dismiss();
            listenerDialog.onSelected(true,tv2.getText().toString());
        }else if(v==tv3){
            dismiss();
            listenerDialog.onSelected(true,tv3.getText().toString());
        }else if(v==tv4){
            dismiss();
            listenerDialog.onSelected(false,tv4.getText().toString());
        }
    }

}

