package com.boncenger.nguberjek.dialogs;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.boncenger.nguberjek.R;

/**
 * Created by adikurniawan on 9/17/16.
 */
@SuppressLint("DefaultLocale")
public class DialogCancelText extends Dialog implements View.OnClickListener {

    EditText editText;
    RelativeLayout btnYa,btnTidak;
    ListenerDialog listenerDialog;

    public DialogCancelText(Context context, ListenerDialog listener){
        super(context, R.style.TransparantDialogTheme);
        this.listenerDialog=listener;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.dialog_cancel_text);

        btnYa=(RelativeLayout)findViewById(R.id.btnYa);
        btnTidak=(RelativeLayout)findViewById(R.id.btnTidak);
        editText=(EditText)findViewById(R.id.editText);


        findViewById(R.id.btnYa).setOnClickListener(this);
        findViewById(R.id.btnTidak).setOnClickListener(this);
    }

    public interface ListenerDialog{
        void onSelected(String value);
    }

    @Override
    public void onClick(View v) {
        if(v==btnYa){
            dismiss();
            listenerDialog.onSelected(editText.getText().toString());
        }else if(v==btnTidak){
            dismiss();

        }
    }

}

