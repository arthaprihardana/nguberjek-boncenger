package com.boncenger.nguberjek.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.helper.ResponseValidation;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.LogManager;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.Session;
import com.boncenger.nguberjek.service.RegistrationIntentService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import cz.msebera.android.httpclient.Header;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sragen on 8/10/2016.
 */
public class RegisterActivity extends BaseActivity {
    private Toolbar toolbar;
    private String jk;
    private TextView etJk;
    private EditText etNama,etUsername,etPhone, etEmail, etPass;
    private ImageView imgProfile;
    LogManager log=new LogManager();

    private Uri mCropImageUri;
    private Uri finalImage;

    // //
    private String imei="";
    private String image_link_uploaded="";
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setToolBar();
        setContext();
        setNavigation();
        startService(new Intent(getBaseContext(), RegistrationIntentService.class));
        if(isImeiPermissionGranted())getIMEI();
    }

    private void setContext(){
        etJk=(TextView)findViewById(R.id.etJk);
        imgProfile=(ImageView)findViewById(R.id.imgProfile);
        etNama=(EditText)findViewById(R.id.etName);
        etUsername=(EditText)findViewById(R.id.etUsername);
        etPhone=(EditText)findViewById(R.id.etNomerTelepon);
        etEmail=(EditText)findViewById(R.id.etEmail);
        etPass=(EditText)findViewById(R.id.etPass);
    }

    private void setNavigation(){

        findViewById(R.id.etJk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Register","click");
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                dialog_gender();

            }
        });
        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareInput();
            }
        });
    }

    private void prepareInput(){
        if(etNama.getText().length()>0 &&
                etUsername.getText().length()>0 &&
                etJk.getText().length()>0 &&
                etPhone.getText().length()>0  &&
                etEmail.getText().length()>0 &&
                etPass.getText().length()>0){
            if(isEmailValid(etEmail.getText().toString())) {
                if(finalImage==null)doRegis();
                else uploadFoto();
            }else{
                dialog_message("Email yang anda masukkan tidak valid.");
            }
        }else{
            dialog_message("Silahkan lengkapi data Anda.");
        }
    }

    public void getIMEI(){
        // GET IMEI //
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        try {
            imei=telephonyManager.getDeviceId();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }





    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    public static class TwitterRestClient {
        private static final String BASE_URL = AppConstants.APIUpload;
        private static AsyncHttpClient client = new AsyncHttpClient();
        public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.get(BASE_URL, params, responseHandler);
        }
        public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
            client.post(BASE_URL, params, responseHandler);
        }
        private static String getAbsoluteUrl(String relativeUrl) {
            return BASE_URL + relativeUrl;
        }
    }

    private void uploadFoto() {
        File myFile = new File(finalImage.getPath());
        RequestParams params = new RequestParams();
        try {
            params.put("userImage", myFile);
        } catch(FileNotFoundException e) {}

        TwitterRestClient.post("uploadImage",params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
                dialog=new ProgressDialog(RegisterActivity.this);
                dialog.setMessage("upload foto");
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // called when response HTTP status is "200 OK"
                //Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_SHORT).show();
                try {
                    Log.d("result",response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    if(status.equals("1")){
                        JSONObject data =response.getJSONObject("dt");
                        image_link_uploaded=data.getString("link");
                        doRegis();
                    }else{
                        dialog_message(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                dialog.dismiss();
                Log.d("result",responseString.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    private void doRegis(){
        final String url= AppConstants.APIRegis;
        String tag_json_obj="do_regis";

        final ProgressDialog dialog= ProgressDialog.show(this, "", "loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        //JSONObject data=response.getJSONObject("dt");
                        goLogin();
                    }else{
                        /*ResponseValidation data = new Gson().fromJson(response.toString(), ResponseValidation.class);
                        String valid_message="";
                        for(int i=0;i<data.err.size();i++){
                            valid_message+=data.err.get(i)+"\n";
                        }*/
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();

                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", etNama.getText().toString());
                params.put("email",etEmail.getText().toString());
                params.put("noTelp",etPhone.getText().toString());
                params.put("jenisKelamin",etJk.getText().toString());
                params.put("username",etUsername.getText().toString());
                params.put("password",etPass.getText().toString());
                params.put("photo",image_link_uploaded);
                params.put("deviceName",getDeviceName());
                params.put("deviceId",imei);
                params.put("senderId",AppConstants.gcm_id);

                log.logD(params.toString());

                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void goLogin(){
        final String url= AppConstants.APILogin;
        String tag_json_obj="Login";

        final ProgressDialog dialog= ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    String TOKEN=response.getString("token");

                    if(status.equals("1")){
                        JSONObject data=response.getJSONObject("dt");


                        createSession(data,TOKEN);
                        Bundle b=new Bundle();
                        b.putBoolean("fromRegister",true);
                        Intent i=new Intent(RegisterActivity.this, HomeAcitivity.class);
                        i.putExtras(b);
                        startActivity(i);
                        finish();
                        ActivityManager.getInstance().finishAll();
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please check your internet connection !" ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", etUsername.getText().toString());
                params.put("password", etPass.getText().toString());
                params.put("senderId", AppConstants.gcm_id);
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void createSession(JSONObject data,String TOKEN)throws JSONException{
        String id=data.getString("_id");
        String username=data.getString("username");
        String nama=data.getString("nama");
        String email=data.getString("email");
        String phone=data.getString("noTelp");
        String gender=data.getString("jenisKelamin");
        String image=data.getString("photo");

        Session sessionModel = new Session();
        sessionModel.setMember_id(id);
        sessionModel.setMember_username(username);
        sessionModel.setMember_password(user.get(SessionManager.KEY_MEMBER_PASSWORD));
        sessionModel.setMember_firstname(nama);
        sessionModel.setMember_email(email);
        sessionModel.setMember_phone(phone);
        sessionModel.setMember_gender(gender);
        sessionModel.setMember_token(TOKEN);
        sessionModel.setMember_foto(image);
        sessionModel.setSender_id(user.get(SessionManager.KEY_SENDER_ID));
        ArrayList<Session> sessionList=new ArrayList<Session>();
        sessionList.add(sessionModel);

        session.logoutUser();
        session.createLoginSession(sessionList);
    }



    private String TAG="PERMISSION";
    public  boolean isImeiPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                //if (ContextCompat.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG,"Permission is granted");
                return true;
            } else {
                Log.v(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }


    private void dialog_gender(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Jenis kelamin Anda? ");
        builder.setPositiveButton("Laki-laki", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                jk = "Laki-laki";
                etJk.setText(jk);
            }
        });
        builder.setNegativeButton("Perempuan", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                jk= "Perempuan";
                etJk.setText(jk);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }




    /**
     * Start pick image activity with chooser.
     */
    public void onSelectImageClick(View view) {
        CropImage.startPickImageActivity(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                finalImage=result.getUri();
                ((ImageView) findViewById(R.id.imgProfile)).setImageURI(result.getUri());
                Glide.with(this)
                        .load(result.getUri())
                        .bitmapTransform(new CropCircleTransformation(this))
                        .into(imgProfile);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            if(requestCode==1){
                getIMEI();
            }else {
                startCropImageActivity(mCropImageUri);
            }
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                //.setMultiTouchEnabled(true)
                .start(this);
    }


    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Buat Akun");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
