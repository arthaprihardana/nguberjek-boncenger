package com.boncenger.nguberjek.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.IntroAdapter;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by sragen on 8/9/2016.
 */
public class IntroPageActivity extends BaseActivity {

    private ViewPager pagerDetail;
    private static int currentPage = 0;
    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        setContent();
        setNavigation();
    }

    private void setNavigation(){
        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IntroPageActivity.this, LoginActivity.class));
            }
        });
        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IntroPageActivity.this, RegisterActivity.class));
            }
        });
    }

    private void setContent(){
        String[] items={
                "<b>Hai, Selamat menggunakan NGUBERJEK!</b><br>Ojek murah berhadiah",
                "<b>Daerah Operasional Terluas</b><br>Tersebar di 30 kota di Indonesia",
                "<b>HADIAH</b><br>Tahunan : Paket Umroh, <br>6 Bulanan : Motor Yamaha MIO-Z, <br>Bulanan : Smartphone Android, <br>Harian : Pulsa Rp. 100.000",
                "<b>Selfie Order</b><br>Apabila anda melihat driver NGUBERJEK, anda dapat memesan langsung menggunakan fitur \"Selfie Order\" di aplikasi rider"
        };
        pagerDetail=(ViewPager)findViewById(R.id.pagerBanner);
        CirclePageIndicator viewPagerIndicator=(CirclePageIndicator)findViewById(R.id.indicator);

        IntroAdapter slideDetailAdapter=new IntroAdapter(IntroPageActivity.this, items);
        pagerDetail.setAdapter(slideDetailAdapter);

        viewPagerIndicator.setViewPager(pagerDetail);
    }

     @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }


    private void startTimer()
    {
        if(timer!=null){
            timer.cancel();
        }
        timer = new CountDownTimer(5000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                currentPage=pagerDetail.getCurrentItem();
            }
            @Override
            public void onFinish() {
                changeSliding();
                Log.i("Timer","Change Pos : "+currentPage);
            }
        };
        timer.start();
    }


    private void changeSliding()
    {
        if(timer!=null){
            timer.cancel();
            startTimer();
        }
        if(pagerDetail!=null){
            currentPage+=1;
            if(currentPage >= 4) {
                currentPage = 0;
                pagerDetail.setCurrentItem(currentPage, true);
            }else{
                pagerDetail.setCurrentItem(currentPage, true);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer!=null){
            timer.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
