package com.boncenger.nguberjek.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.ConvertRupiah;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.MenuModel;
import com.boncenger.nguberjek.model.OrderModel;
import com.boncenger.nguberjek.model.RiderModel;
import com.bumptech.glide.Glide;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by adikurniawan on 11/19/17.
 */

public class WaitingFoodActivity extends BaseActivity {

    // SOCKET APP //
    private Socket mSocket;

    // SET CONTENT //
    private RelativeLayout container_proses, container_result, container_image;
    private ImageView img_rider_bg, img_rider;
    private TextView tv_lokasi_awal, tv_lokasi_tujuan, tv_name, tv_nopol, tv_phone, tv_fare, tv_message, tv_title;
    private LinearLayout btn;
    private OrderModel dataOrder;
    private RiderModel data;
    private RotateLoading rotateLoading;
    private SmoothProgressBar progressIndicator;

    private String order_id="";

    private Handler timer;

    private ArrayList<MenuModel> menuKeranjang=new ArrayList<MenuModel>();


    // BUNDLE //
    Bundle b;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting);


        /// SOCKET ///
        AppController app = (AppController) getApplication();
        mSocket = app.getSocket();
        //////////////

        timer = new Handler();

        setContent();
        setNavigation();
        socketOff();
        socketOn();
        b=getIntent().getExtras();
        if(b!=null){
            dataOrder=(OrderModel) b.getSerializable("data");
            menuKeranjang=(ArrayList<MenuModel>)b.getSerializable("dataKeranjang");
            emit_OrderRequest();
        }
    }

    private void proses(){
        rotateLoading.start();
        container_proses.setVisibility(View.VISIBLE);
        container_image.setVisibility(View.GONE);
    }

    private void result(){
        rotateLoading.stop();
        container_proses.setVisibility(View.GONE);
        container_image.setVisibility(View.VISIBLE);
    }

    private void socketOn(){
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("server-response: food order request", onOrderRequest);
        mSocket.on("server-response: order food status", onOrderRequest);
        mSocket.connect();
    }

    private void socketOff(){
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT,onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("server-response: food order request", onOrderRequest);
        mSocket.off("server-response: order food status", onOrderStatus);
    }

    private void emit_OrderRequest(){
        proses();
        JSONObject data=new JSONObject();
        try {
            data.put("token", user.get(SessionManager.KEY_MEMBER_TOKEN));
            data.put("boncengerId", user.get(SessionManager.KEY_MEMBER_ID));

            data.put("namaResto",dataOrder.order_resto_id);
            data.put("koordinatResto",dataOrder.order_longitude_awal+","+dataOrder.order_latitude_awal);
//            data.put("koordinatResto",106.7516676+","+-6.2388328); // rumah
//            data.put("koordinatResto",106.7845103+","+-6.207081); // kantor

            data.put("alamatPengantaran", dataOrder.order_location_tujuan);
            data.put("koordinatPengantaran", dataOrder.order_longitude_tujuan+","+dataOrder.order_latitude_tujuan);

            data.put("perkiraanHarga",dataOrder.order_price_food);
            data.put("biayaAntar",dataOrder.order_price_delivery);
            data.put("totalHarga",dataOrder.order_price);

            data.put("jarak",dataOrder.order_distance);
            data.put("waktu",dataOrder.order_duration);
            data.put("kategori",dataOrder.order_service);

            JSONArray namaMenu=new JSONArray();
            for (int i=0;i<menuKeranjang.size();i++) {
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("menuId",menuKeranjang.get(i).id);
                jsonObject.put("harga",menuKeranjang.get(i).price);
                jsonObject.put("jmlPorsi",menuKeranjang.get(i).qty);
                int subTotal= menuKeranjang.get(i).price * menuKeranjang.get(i).qty;
                jsonObject.put("subTotal",subTotal);
                jsonObject.put("catatan",menuKeranjang.get(i).note);
                namaMenu.put(jsonObject);
            }
            data.put("namaMenu",namaMenu);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: food order request", data);
        Log.d("SOCKET","client-request: food order request");
        Log.d("SOCKET",data.toString());

    }

    private Emitter.Listener onOrderRequest = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        result();
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET","server-response: food order request");
                        Log.d("SOCKET",response.toString());

                        String status = response.getString("s");
                        String message = response.getString("message");

                        if(status.equals("1")){
                            JSONObject dataObj = response.getJSONObject("data");
                            RiderModel dataRider = new RiderModel(dataObj);
                            if(!dataObj.isNull("orderId")){
                                order_id=dataObj.getString("orderId");

                                timer.postDelayed(timerStatus,1000); // 1 second delay (takes millis)

                            }
                            data=dataRider;
                            setData(message);
                        }else{
                            setDataNoRider(message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            });
        }
    };

    private Runnable timerStatus = new Runnable(){
        private long time = 0;
        @Override
        public void run() {
            //Do Something
            mSocket.off("server-response: order food status", onOrderStatus);
            mSocket.on("server-response: order food status", onOrderStatus);
            emit_OrderStatus();

            // do stuff then
            // can call h again after work!
            time += 1000;
            Log.d("TimerExample", "Going for... " + time);
            timer.postDelayed(this, 1000);
        }
    };



    private void emit_OrderStatus(){
        JSONObject data=new JSONObject();
        try {
            data.put("order_id", order_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: order food status", data);
        Log.d("SOCKET","client-request: order food status");
        Log.d("SOCKET",data.toString());
    }


    private Emitter.Listener onOrderStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        result();
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET","server-response: order food status");
                        Log.d("SOCKET",response.toString());

                        String status = response.getString("status");
                        String message = response.getString("message");
                        String order_id = response.getString("orderId");

                        if(status.equals("1")) {
                            timer.removeCallbacks(timerStatus);

                            Intent i = new Intent(WaitingFoodActivity.this, TrackingActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("order_id", order_id);
                            bundle.putBoolean("dalam_perjalanan", false);
                            i.putExtras(bundle);
                            startActivity(i);

                            ActivityManager.getInstance().finishAll();
                            finish();
                            socketOff();
                        }

                    } catch (JSONException e){
                        e.printStackTrace();
                        return;
                    }


                }
            });
        }
    };

    private void setContent(){
        container_proses=(RelativeLayout)findViewById(R.id.container_proses);
        container_result=(RelativeLayout)findViewById(R.id.container_result);
        container_image=(RelativeLayout)findViewById(R.id.container_image);
        rotateLoading=(RotateLoading)findViewById(R.id.rotateloading);
        progressIndicator=(SmoothProgressBar)findViewById(R.id.progress);
        btn=(LinearLayout)findViewById(R.id.btn);
        img_rider_bg=(ImageView)findViewById(R.id.image_rider_bg);
        img_rider=(ImageView)findViewById(R.id.image_rider);
        tv_lokasi_awal=(TextView)findViewById(R.id.autoCompleteLokasiAwal);
        tv_lokasi_tujuan=(TextView)findViewById(R.id.autoCompleteLokasiTujuan);
        tv_name=(TextView)findViewById(R.id.tv_name);
        tv_nopol=(TextView)findViewById(R.id.tv_nopol);
        tv_phone=(TextView)findViewById(R.id.tv_phone);
        tv_fare=(TextView)findViewById(R.id.tv_fare);
        tv_message=(TextView)findViewById(R.id.tv_message);
        tv_title=(TextView)findViewById(R.id.mencari);

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_exit("Batalkan Order?");
            }
        });
    }

    private void setNavigation(){
        findViewById(R.id.btnYa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiCancelOrder("",true);
            }
        });
        findViewById(R.id.btnTidak).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                apiCancelOrder("",false);
            }
        });

        findViewById(R.id.tv_fare).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Bundle b=new Bundle();
                b.putString("order_id",order_id);
                Intent intent=new Intent(WaitingActivity.this, TestSocket.class);
                intent.putExtras(b);
                startActivity(intent);*/
            }
        });
    }

    private void setDataNoRider(final String message){
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                btn.setVisibility(View.GONE);
                container_proses.setVisibility(View.VISIBLE);
                container_image.setVisibility(View.GONE);
                progressIndicator.setVisibility(View.VISIBLE);
                tv_title.setText("Mohon tunggu\nOrder Anda sedang diproses oleh Rider/Driver...");
            }

            public void onFinish() {
                btn.setVisibility(View.VISIBLE);
                container_proses.setVisibility(View.VISIBLE);
                container_image.setVisibility(View.GONE);
                progressIndicator.setVisibility(View.GONE);
                tv_title.setText(message);
            }
        }.start();



    }

    private void setData(String message){
        String lokasiAwal=dataOrder.order_location_awal;
        String lokasiTujuan=dataOrder.order_location_tujuan;
        String price=dataOrder.order_price;

        try {
            Glide.with(WaitingFoodActivity.this)
                    .load(data.rider_photo)
                    .placeholder(R.mipmap.icon_profil)
                    .bitmapTransform(new CropCircleTransformation(this))
                    .into(img_rider);

            Glide.with(WaitingFoodActivity.this)
                    .load(data.rider_photo)
                    .placeholder(R.mipmap.icon_profil)
                    .bitmapTransform(new BlurTransformation(this))
                    .into(img_rider_bg);
        }catch (Throwable throwable){
            Log.d(WaitingActivity.class.getSimpleName(),"error load image"+throwable.toString());
        }

        Log.d("lokasi",lokasiAwal);
        Log.d("lokasi",lokasiTujuan);
        tv_lokasi_awal.setText(lokasiAwal);
        tv_lokasi_tujuan.setText(lokasiTujuan);
        tv_name.setText(data.rider_name);
        tv_nopol.setText(data.rider_kendaraan+", "+data.rider_nopol);
        tv_phone.setText(data.rider_nopol);
        tv_message.setText(message);
        ConvertRupiah rp=new ConvertRupiah();
        try {
            tv_fare.setText("HARGA : Rp." + rp.ConvertRupiah(price));
        }catch (Throwable throwable){}
    }


    private void apiCancelOrder(final String alasan, final boolean isReOrder){
        final String url= AppConstants.APIUpdateStatusFood;
        log.logD(url);
        String tag_json_obj="update_status";

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");

                    if(status.equals("1")){
                        if(isReOrder) {
                            btn.setVisibility(View.GONE);
                            container_proses.setVisibility(View.VISIBLE);
                            container_image.setVisibility(View.GONE);
                            progressIndicator.setVisibility(View.VISIBLE);
                            tv_title.setText("Mencari Rider");


                            socketOff();
                            socketOn();
                            emit_OrderRequest();
                        }else{
                            startActivity(new Intent(WaitingFoodActivity.this, HomeAcitivity.class));
                            ActivityManager.getInstance().finishAll();
                            finish();
                            socketOff();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId",order_id);
                params.put("status","6");
                params.put("komentar", alasan);
                params.put("rating", "0");
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dialog_exit("Batalkan Order?");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socketOff();
        timer.removeCallbacks(timerStatus);

    }

    public void dialog_exit(String message){
        final AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                if(order_id.equals("")){
                    finish();
                }else {
                    apiCancelOrder("", false);
                }
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
