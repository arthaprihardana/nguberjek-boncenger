package com.boncenger.nguberjek.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.model.RiderModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.boncenger.nguberjek.adapter.MenuAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.app.NavigationDrawer;
import com.boncenger.nguberjek.dialogs.DialogCancelOrder;
import com.boncenger.nguberjek.dialogs.DialogCancelText;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.helper.ConvertRupiah;
import com.boncenger.nguberjek.helper.GPSTracker;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sragen on 8/20/2016.
 */
public class TrackingActivity extends BaseActivity implements DirectionCallback, OnMapReadyCallback  {

    Toolbar toolbar;
    DrawerLayout drawer;

    public static boolean mMapIsTouched = false;


    private ImageView imgProfile;
    private TextView tvUser;
    private TextView tvPhone;

    private String order_id;
    private boolean dalam_perjalanan=false;

    private ProgressDialog dialog;
    private OrderModel dataOrder;

    // for map //
    private static LatLng CURRENT_lOCATION ;
    private static LatLng DESTINATION_lOCATION;
    private double latitude,longitude;
    private GoogleMap map;
    private UiSettings mUiSettings;
    private GPSTracker gps;
    // private static final String API_KEY = "AIzaSyCbj0WG88GvLFjyxNTYBpXGvaO9y_AK6U0";    // API KEY DEVELOPMENT
    private static final String API_KEY = "AIzaSyDnTce5NZQrxVHh6r7hLkAdeTF9VU8Adz4";     // API KEY PRODUCTION
    private LatLng camera = new LatLng(0, 0);
    private LatLng origin = new LatLng(0, 0);
    private LatLng destination = new LatLng(0, 0);

    // SOCKET APP //
    private Socket mSocket;

    private boolean isFood=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking);


        Bundle b=getIntent().getExtras();
        order_id=b.getString("order_id");
        dalam_perjalanan=b.getBoolean("dalam_perjalanan");


        if(order_id.contains("food_"))isFood=true;

        try {
            if (b.getString("message")!=null)
                dialog_message2("", b.getString("message"));
        }catch (Throwable throwable){}

        try{
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.framentMap);
            mapFragment.getMapAsync(this);
        } catch (Throwable tr) {}


        setToolBar();
        setNavigationDrawer();
        setContent();
        setNavigation();

        getData();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (isMapPermissionGranted()) setMap();

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);
    }



    public void dialog_message2(final String title,final String message){
        try {
            new CountDownTimer(3000, 1000) {
                public void onTick(long millisUntilFinished) {
                    //here you can have your logic to set text to edittext
                }
                public void onFinish() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
                    builder.setTitle(title);
                    builder.setMessage(message);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    builder.show();
                }

            }.start();

        }catch (Throwable throwable){}
    }

    public void setMap(){
        CURRENT_lOCATION=new LatLng(dataOrder.order_latitude_awal,dataOrder.order_longitude_awal);
        DESTINATION_lOCATION=new LatLng(dataOrder.order_latitude_tujuan,dataOrder.order_longitude_tujuan);
        map.clear();

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 13));
        map.animateCamera(CameraUpdateFactory.zoomTo(13), 2000, null);

        requestDirection();
    }

    private void getData(){
        final String url= AppConstants.APIDetailOrder+"?orderId="+order_id;
        log.logD(url);
        String tag_json_obj="get_data_order";
        dialog= ProgressDialog.show(TrackingActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        JSONArray dataArray=response.getJSONArray("dt");
                        if(dataArray.length()>0) {
                            JSONObject data = dataArray.getJSONObject(0);
                            if(isFood){
                                dataOrder=new OrderModel(data, true);
                            }else {
                                dataOrder = new OrderModel(data);
                            }
                            setData();
                            setMap();
                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void cancelOrder(final String alasan){
        final String url= AppConstants.APIUpdateStatus;
        log.logD(url);
        String tag_json_obj="update_status";
        dialog= ProgressDialog.show(TrackingActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");

                    if(status.equals("1")){
                        startActivity(new Intent(TrackingActivity.this,HomeAcitivity.class));
                        ActivityManager.getInstance().finishAll();
                        finish();
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId",order_id);
                params.put("status","6");
                params.put("komentar", alasan);
                params.put("riderId", dataOrder.rider_id);
                params.put("rating", "");
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void setContent(){
        if(dalam_perjalanan){
            findViewById(R.id.rlSelamat).setVisibility(View.VISIBLE);
            findViewById(R.id.rlTracking).setVisibility(View.GONE);
        }else {
            findViewById(R.id.rlSelamat).setVisibility(View.GONE);
            findViewById(R.id.rlTracking).setVisibility(View.VISIBLE);
        }
    }

    private void setNavigation(){
        findViewById(R.id.imageRider).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        findViewById(R.id.btnChatting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(TrackingActivity.this, ChatActivity.class);
                Bundle b=new Bundle();
                b.putString("order_id",order_id);
                i.putExtras(b);
                startActivity(i);
                //sendSMSMessage(dataOrder.rider_phone);
            }
        });
        findViewById(R.id.btnCalling).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call();
            }
        });
        findViewById(R.id.btnCanceling).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_cancel();
            }
        });
        findViewById(R.id.imageGedung).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    protected void sendSMSMessage(String phone) {
        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", phone);
        smsIntent.putExtra("sms_body","");
        startActivity(smsIntent);
    }


    private void call(){
        String number_phone = dataOrder.rider_phone;
        number_phone = number_phone.replace(" ", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number_phone + ""));
        if (ActivityCompat.checkSelfPermission(TrackingActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }


    private void setData(){
        ImageView imageRider=(ImageView)findViewById(R.id.imageRider);
        TextView tvName=(TextView)findViewById(R.id.tvName);
        TextView tvNopol=(TextView)findViewById(R.id.tvNopol);
        TextView tvLocationAwal=(TextView)findViewById(R.id.tvLokasiAwal);
        TextView tvLocationTujuan=(TextView)findViewById(R.id.tvLokasiTujuan);
        TextView tvDistance=(TextView)findViewById(R.id.tvDistance);
        TextView tvPrice=(TextView)findViewById(R.id.tvPrice);
        LinearLayout lnRating=(LinearLayout)findViewById(R.id.lnRating);

        lnRating.removeAllViews();
        ImageView imgRating = null;
        for (int i = 1; i <= 5; i++) {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    35,
                    35);

            imgRating = new ImageView(this);
            imgRating.setLayoutParams(params);
            //imgRating.setId(i);
            if(i<=dataOrder.rider_rating)imgRating.setImageResource(R.mipmap.star);
            else imgRating.setImageResource(R.mipmap.star_line);
            lnRating.addView(imgRating);
        }

        Glide.with(this)
                .load(dataOrder.rider_photo)
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imageRider);

        ConvertRupiah rp=new ConvertRupiah();
        tvName.setText(dataOrder.rider_name);
        tvNopol.setText(dataOrder.rider_nopol);
        tvLocationAwal.setText(dataOrder.order_location_awal);
        tvLocationTujuan.setText(dataOrder.order_location_tujuan);
        tvDistance.setText(dataOrder.order_distance+" KM");
        tvPrice.setText("Rp. "+rp.ConvertRupiah(dataOrder.order_price));
    }


    public void requestDirection() {
        GoogleDirection.withServerKey(API_KEY)
                .from(CURRENT_lOCATION)
                .to(DESTINATION_lOCATION)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {
            map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));
            map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));

            // get distance  and  duration//
            String distance=direction.getRouteList().get(0).getLegList().get(0).getDistance().getText();
            String duration=direction.getRouteList().get(0).getLegList().get(0).getDuration().getText();


            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.RED));

            //map.moveCamera(CameraUpdateFactory.newLatLngZoom(DESTINATION_lOCATION, 13));
            //map.animateCamera(CameraUpdateFactory.zoomOut(), 2000, null);
        }else{
            Toast.makeText(getApplicationContext(),"direction not ok",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.d("direction",t.getMessage());
        //Snackbar.make(btnRequestDirection, t.getMessage(), Snackbar.LENGTH_SHORT).show();
    }



    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private void grantPermission() {
        int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[] {android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CODE_ASK_PERMISSIONS);
            return;
        }
    }

    String TAG="permission";
    public boolean isMapPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if(grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // Permission Granted
                        finish();
                        startActivity(new Intent(this,TrackingActivity.class));
                    } else {
                        // Permission Denied
                        //Toast.makeText(this, "Denied", Toast.LENGTH_SHORT).show();
                        //setContent();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }




    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
           /* actionBar.setHomeAsUpIndicator(R.drawable.back);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
*/
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("NGUBERJEK");


            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDataProfile();
    }
    private void showDataProfile(){
        session=new SessionManager(this);
        user=session.getUserDetails();
        Glide.with(this)
                .load(user.get(SessionManager.KEY_MEMBER_IMAGE))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imgProfile);
        tvUser.setText(user.get(SessionManager.KEY_MEMBER_FIRSTNAME));
        tvPhone.setText(user.get(SessionManager.KEY_MEMBER_PHONE));
    }

    private void setNavigationDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        View headerView = View.inflate(TrackingActivity.this, R.layout.nav_header_main, null);
        ListView listView=(ListView)findViewById(R.id.listView);
        imgProfile=(ImageView)headerView.findViewById(R.id.circleView);
        tvUser=(TextView)headerView.findViewById(R.id.tvName);
        tvPhone=(TextView)headerView.findViewById(R.id.tvPhone);
        headerView.findViewById(R.id.imgPencil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrackingActivity.this, EditProfileActivity.class));
            }
        });
        findViewById(R.id.tvTentangKami).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TrackingActivity.this, TentangKamiActivity.class));
            }
        });
        findViewById(R.id.tvLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                dialog_logout();
            }
        });

        listView.addHeaderView(headerView,null, false);
        MenuAdapter menuAdapter=new MenuAdapter(TrackingActivity.this);
        listView.setAdapter(menuAdapter);

        Glide.with(this)
                .load(user.get(SessionManager.KEY_MEMBER_IMAGE))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imgProfile);
        tvUser.setText(user.get(SessionManager.KEY_MEMBER_FIRSTNAME));
        tvPhone.setText(user.get(SessionManager.KEY_MEMBER_PHONE));


        NavigationDrawer drawerClick=new NavigationDrawer(this,listView,drawer);

    }


    private void dialog_logout(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Apakah anda yakin ingin keluar ? ");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                session.logoutUser();
                doLogout();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void dialog_cancel(){
//        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Anda yakin ingin membatalkan perjalanan ? ");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                    dialog_cancel2();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            });
            builder.setCancelable(false);
            builder.show();
//        }catch (Throwable throwable){}
    }

    private void dialog_cancel2(){
//        try {
            DialogCancelOrder dial = new DialogCancelOrder(context, new DialogCancelOrder.ListenerDialog() {
                @Override
                public void onSelected(boolean canInput, String value) {
                    if (canInput) {
                        cancelOrder(value);
                    } else {
                        dialog_cancel3();
                    }
                }
            });
            dial.show();
//        }catch (Throwable throwable){}
    }

    private void dialog_cancel3(){
//        try {
            DialogCancelText dial = new DialogCancelText(context, new DialogCancelText.ListenerDialog() {
                @Override
                public void onSelected(String value) {
                    cancelOrder(value);
                }
            });
            dial.show();
//        }catch (Throwable throwable){}
    }

    private void doLogout(){
        final String url= AppConstants.APILogout+"?_id="+user.get(SessionManager.KEY_MEMBER_ID);
        String tag_json_obj="Logout";

        final ProgressDialog dialog= ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        session.logoutUser();
                        finish();
                        ActivityManager.getInstance().finishAll();
                        startActivity(new Intent(TrackingActivity.this, IntroPageActivity.class));
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please check your internet connection !" ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Access-Token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        //finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }



}

