package com.boncenger.nguberjek.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.ConfirmCartAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.model.AddressModel;
import com.boncenger.nguberjek.model.MenuModel;
import com.boncenger.nguberjek.model.OrderModel;
import com.boncenger.nguberjek.model.RestoModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by adikurniawan on 11/11/17.
 */

public class ConfirmOrderFoodActivity extends BaseActivity {

    private ArrayList<MenuModel> menuKeranjangBefore=new ArrayList<MenuModel>();
    private ArrayList<MenuModel> menuKeranjang=new ArrayList<MenuModel>();
    private ConfirmCartAdapter adapter;
    private RestoModel dataResto;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_food);


        // reset variable global //
        AppConstants.addressModel=new AddressModel();
        // end reset //


        getBundle();
        setContent();
        setNavigation();
        setToolBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(AppConstants.addressModel.my_lat!=0 && AppConstants.addressModel.my_lng!=0){
            adapter.notifyDataSetChanged();
        }
    }

    private void getBundle(){
        Bundle b=getIntent().getExtras();
        dataResto=(RestoModel)b.getSerializable("dataResto");
        menuKeranjangBefore=(ArrayList<MenuModel>)b.getSerializable("menuCart");
        log.logD(menuKeranjangBefore.size()+"");

        AppConstants.addressModel.resto_address_name=dataResto.address;
        AppConstants.addressModel.resto_lat=dataResto.lat;
        AppConstants.addressModel.resto_lng=dataResto.lng;

        for(int i=0;i<menuKeranjangBefore.size();i++){
            if(menuKeranjangBefore.get(i).qty>0) {
                menuKeranjang.add(menuKeranjangBefore.get(i));
            }
        }
    }

    private void setContent(){
        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        adapter=new ConfirmCartAdapter(this, menuKeranjang, AppConstants.addressModel);
        recyclerView.setAdapter(adapter);
    }


    private void setNavigation(){
        findViewById(R.id.btnOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!AppConstants.addressModel.my_address_name.equals(""))
                    submitOrder();
                else
                    Toast.makeText(getApplicationContext(), "Anda belum memilih alamat !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateCart(ArrayList<MenuModel> menuList){
        int count=0;
        int price=0;
        for(int i=0;i<menuList.size();i++){
            count=count+menuList.get(i).qty;
            price=price+(menuList.get(i).qty * menuList.get(i).price);
        }
    }

    public void submitOrder(){

        int total=0;
        int price=0;
        int priceDelivery=0;
        for(int x=0;x<menuKeranjang.size();x++){
            price=price+(menuKeranjang.get(x).qty * menuKeranjang.get(x).price);
        }

        try{
            priceDelivery= Integer.parseInt(AppConstants.addressModel.price);
        }catch (Throwable throwable){}
        total=price+priceDelivery;



        OrderModel orderModel = new OrderModel();
        orderModel.order_resto_id=dataResto.id;
        orderModel.order_location_awal = dataResto.address;
        orderModel.order_longitude_awal = dataResto.lng;
        orderModel.order_latitude_awal = dataResto.lat;
        orderModel.order_location_tujuan = AppConstants.addressModel.my_address_name;
        orderModel.order_longitude_tujuan = AppConstants.addressModel.my_lng;
        orderModel.order_latitude_tujuan = AppConstants.addressModel.my_lat;
        orderModel.order_price = total+"";
        orderModel.order_price_delivery=priceDelivery+"";
        orderModel.order_price_food=price+"";
        orderModel.order_distance = AppConstants.addressModel.distance;
        orderModel.order_duration = AppConstants.addressModel.duration;
        orderModel.order_service = "nguberjek";


        Bundle b = new Bundle();
        b.putSerializable("data", orderModel);
        b.putSerializable("dataKeranjang", menuKeranjang);
        Intent intent = new Intent(ConfirmOrderFoodActivity.this, WaitingFoodActivity.class);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void goLocation(){
        Bundle b=new Bundle();
        b.putDouble("lat_resto",dataResto.lat);
        b.putDouble("lng_resto",dataResto.lng);
        Intent intent=new Intent(this,Location_Activity.class);
        intent.putExtras(b);
        startActivity(intent);
    }





    private void setToolBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Booking Confirmation");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
