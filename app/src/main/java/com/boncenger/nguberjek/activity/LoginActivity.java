package com.boncenger.nguberjek.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.model.Session;
import com.boncenger.nguberjek.service.RegistrationIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sragen on 8/9/2016.
 */
public class LoginActivity extends BaseActivity {

    private Toolbar toolbar;
    private EditText etEmail,etPass;
    private String TOKEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setToolBar();
        setContent();
        setNavigation();
        startService(new Intent(getBaseContext(), RegistrationIntentService.class));
    }

    private void setContent(){
        etEmail=(EditText)findViewById(R.id.etEmail);
        etPass=(EditText)findViewById(R.id.etPass);
    }

    private void setNavigation(){
        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareInput();
            }
        });
        findViewById(R.id.btnRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
        findViewById(R.id.tvForgetPass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetPassActivity.class));
            }
        });
    }



    private void prepareInput(){
        if(etEmail.getText().toString().length()>0 &&
                etPass.getText().toString().length()>0){
            goLogin();
        }else {
            dialog_message("Masukkan email dan password Anda.");
        }
    }

    private void goLogin(){
        final String url= AppConstants.APILogin;
        String tag_json_obj="Login";

        final ProgressDialog dialog= ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    dialog.dismiss();
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");
                    if(status.equals("1")){
                        TOKEN=response.getString("token");
                        JSONObject data=response.getJSONObject("dt");

                        createSession(data);
                        startActivity(new Intent(LoginActivity.this, HomeAcitivity.class));
                        finish();
                        ActivityManager.getInstance().finishAll();
                    }else{
                        dialog_message(message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        "Please check your internet connection !" ,
                        Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username",etEmail.getText().toString() );
                params.put("password", etPass.getText().toString());
                params.put("senderId", AppConstants.gcm_id);
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void createSession(JSONObject data)throws JSONException{
        String id=data.getString("_id");
        String username=data.getString("username");
        String nama=data.getString("nama");
        String email=data.getString("email");
        String phone=data.getString("noTelp");
//        String gender=data.getString("jenisKelamin");
        String gender="perempuan";
        String image=data.getString("photo");

        Session sessionModel = new Session();
        sessionModel.setMember_id(id);
        sessionModel.setMember_username(username);
        sessionModel.setMember_password(etPass.getText().toString());
        sessionModel.setMember_firstname(nama);
        sessionModel.setMember_email(email);
        sessionModel.setMember_phone(phone);
        sessionModel.setMember_gender(gender);
        sessionModel.setMember_token(TOKEN);
        sessionModel.setMember_foto(image);
        sessionModel.setSender_id(AppConstants.gcm_id);
        ArrayList<Session>sessionList=new ArrayList<Session>();
        sessionList.add(sessionModel);

        session.logoutUser();
        session.createLoginSession(sessionList);
    }




    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Masuk");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
