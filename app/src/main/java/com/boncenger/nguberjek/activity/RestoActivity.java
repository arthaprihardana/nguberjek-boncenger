package com.boncenger.nguberjek.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.KategoriFoodAdapter;
import com.boncenger.nguberjek.adapter.RestoAdapter;
import com.boncenger.nguberjek.app.API;
import com.boncenger.nguberjek.app.ErrorCallback;
import com.boncenger.nguberjek.app.ResponseCallback;
import com.boncenger.nguberjek.model.CategoryModel;
import com.boncenger.nguberjek.model.RestoModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by adikurniawan on 10/18/17.
 */

public class RestoActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private String title="";
    private ProgressBar progressBar;
    private RecyclerView rView;
    private ArrayList<RestoModel> rowList=new ArrayList<RestoModel>();
    private RestoAdapter rcAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto);

        title= getIntent().getExtras().getString("title");

        setToolbar();
        setContent();
    }

    private void setContent(){
        LinearLayoutManager lLayout = new LinearLayoutManager(this);

        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        rView = (RecyclerView)findViewById(R.id.recyclerView);
        rView.setNestedScrollingEnabled(false);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);

        final EditText etSearch=(EditText)findViewById(R.id.et_search);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getResto(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        getResto("");

    }

    public void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        rView.setVisibility(View.GONE);
    }
    public void hideLoading(){
        progressBar.setVisibility(View.GONE);
        rView.setVisibility(View.VISIBLE);
    }

    public void getResto(String keyword){
        showLoading();
        API.getResto(this,keyword, new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        try {
                            hideLoading();
                            rowList.clear();
                            JSONObject data_response = new JSONObject(response);
                            String status = data_response.getString("s");
                            String message = data_response.getString("msg");
                            JSONArray data_array = data_response.getJSONArray("dt");

                            for(int i=0;i<data_array.length();i++){
                                JSONObject data=data_array.getJSONObject(i);
                                rowList.add(new RestoModel(data));
                            }
                            setData();
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        return httpCode;
                    }
                },
                new ErrorCallback() {
                    @Override
                    public void doAction() {
                        try {
                            hideLoading();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void setData(){
        rcAdapter = new RestoAdapter(this, rowList);
        rView.setAdapter(rcAdapter);
    }

    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText(title);

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
