package com.boncenger.nguberjek.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.API;
import com.boncenger.nguberjek.app.ErrorCallback;
import com.boncenger.nguberjek.app.ResponseCallback;
import com.boncenger.nguberjek.fragment.MenuFragment;
import com.boncenger.nguberjek.model.CategoryModel;
import com.boncenger.nguberjek.model.MenuModel;
import com.boncenger.nguberjek.model.RestoModel;
import com.bumptech.glide.Glide;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by adikurniawan on 10/19/17.
 */

public class RestoMenuActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private Adapter adapterFragment;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private ArrayList<CategoryModel> categoryList=new ArrayList<CategoryModel>();
    private TextView tvCartCount;
    private TextView tvCartPrice;

    private RestoModel dataResto;
    private String title="";
    private ArrayList<MenuModel> menuCart=new ArrayList<MenuModel>();
    private int count=0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_menu);

        Bundle b=getIntent().getExtras();
        dataResto=(RestoModel)b.getSerializable("data_resto");
        if(dataResto!=null)title=dataResto.name;

        setToolBar();
        setContent();
        setNavigation();
        getMenu();
    }

    private void setContent(){
        tvCartCount=(TextView)findViewById(R.id.tvCartCount);
        tvCartPrice=(TextView)findViewById(R.id.tvCartPrice);

        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        ImageView imgToolbar=(ImageView)findViewById(R.id.imageToolbar);
        TextView tvName=(TextView)findViewById(R.id.tvName);
        TextView tvAddress=(TextView)findViewById(R.id.tvAddress);
        Button btnStatus=(Button) findViewById(R.id.btnStatus);
        TextView tvOpeningHours=(TextView)findViewById(R.id.tvOpeningHours);
        if(dataResto!=null) {
            tvName.setText(dataResto.name);
            tvAddress.setText(dataResto.address);
            if(dataResto.status.equals("true"))
                btnStatus.setText("OPEN");
            else
                btnStatus.setText("CLOSED");
            tvOpeningHours.setText(dataResto.openingHours);
            Glide.with(this)
                    .load(dataResto.image)
                    .placeholder(R.drawable.img_placeholder)
                    .into(imgToolbar);
        }
    }

    private void setNavigation(){
        findViewById(R.id.contanerInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataResto!=null) {
                    Bundle b = new Bundle();
                    b.putSerializable("data_resto", dataResto);
                    Intent intent = new Intent(RestoMenuActivity.this, RestoDetailActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                }
            }
        });
        findViewById(R.id.btnOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(count>0) {
                    Bundle b = new Bundle();
                    b.putSerializable("menuCart", menuCart);
                    b.putSerializable("dataResto",dataResto);
                    Intent intent = new Intent(RestoMenuActivity.this, ConfirmOrderFoodActivity.class);
                    intent.putExtras(b);
                    startActivity(intent);
                }else{
                    Toast.makeText(RestoMenuActivity.this, "Keranjang belanja kosong !", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }
    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
    }

    public void getMenu(){
        showLoading();
        API.getMenu(this, dataResto.id, new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        try {
                            hideLoading();
                            JSONObject data_response = new JSONObject(response);
                            String status = data_response.getString("s");
                            String message = data_response.getString("msg");
                            if(status.equals("1")) {
                                JSONArray data_array = data_response.getJSONArray("dt");
                                for(int i=0;i<data_array.length();i++){
                                    JSONObject dataKategori=data_array.getJSONObject(i);
                                    categoryList.add(new CategoryModel(dataKategori,true));
                                }
                            }else{
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }



                            setPager();
                        } catch (JSONException e) {
                            Log.e("error",e.toString());
                        }
                        return httpCode;
                    }
                },
                new ErrorCallback() {
                    @Override
                    public void doAction() {
                        try {
                            hideLoading();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void setPager(){
        adapterFragment = new Adapter(getSupportFragmentManager());
        for(int i=0;i<categoryList.size();i++){
            MenuFragment menuFragment = new MenuFragment();

            Bundle args = new Bundle();
            args.putSerializable("data_menu", categoryList.get(i).menuList);
            menuFragment.setArguments(args);

            adapterFragment.addFragment(menuFragment, categoryList.get(i).name);
        }


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(adapterFragment);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    public void updateCart(ArrayList<MenuModel> menuList){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        int count=0;
        int price=0;
        for(int i=0;i<menuList.size();i++){
            count=count+menuList.get(i).qty;
            price=price+(menuList.get(i).qty * menuList.get(i).price);
        }
        this.menuCart=menuList;
        this.count=count;
        tvCartCount.setText(""+count);
        tvCartPrice.setText(""+formatRupiah.format(price));
    }








    private void setToolBar(){
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            //actionBar.setTitle("Mister Lie Palmerah");
        }

        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbarLayout);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(title);
                    //collapsingToolbarLayout.setCollapsedTitleTextColor(Color.parseColor("#ffffff"));
                    isShow = true;
                } else if(isShow) {
                    collapsingToolbarLayout.setTitle("");//carefull there should a space between double quote otherwise it wont work
                    //collapsingToolbarLayout.setCollapsedTitleTextColor(Color.parseColor("#003b3b3b"));
                    isShow = false;
                }

            }
        });

        /*View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_custom, null);
        android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT,
                ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER);

        actionBar.setCustomView(mCustomView, params);
        actionBar.setDisplayShowCustomEnabled(true);
        TextView tv_title=(TextView)mCustomView.findViewById(R.id.tv_title);
        tv_title.setText("Akun Saya");
        tv_title.setVisibility(View.VISIBLE);*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
