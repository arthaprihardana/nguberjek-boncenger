package com.boncenger.nguberjek.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.helper.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by adikurniawan on 7/8/17.
 */

public class TestSocket extends BaseActivity {
    private Socket mSocket;
    private String order_id="";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b=getIntent().getExtras();
        order_id=b.getString("order_id");

        /// SOCKET ///
        AppController app = (AppController) getApplication();
        mSocket = app.getSocket();
        //////////////

        socketOff();
        socketOn();
        emit_OrderStatus();
    }

    private void socketOn(){
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("server-response: order status", onOrderStatus);
        mSocket.connect();
    }

    private void socketOff(){
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT,onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("server-response: order status", onOrderStatus);
    }

    private void emit_OrderStatus(){
        JSONObject data=new JSONObject();
        try {
            data.put("order_id", order_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: order status", data);
        Log.d("SOCKET","client-request: order status");
        Log.d("SOCKET",data.toString());
    }


    private Emitter.Listener onOrderStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET","server-response: order status");
                        Log.d("SOCKET",response.toString());

                        //String message = response.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }

                }
            });
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
