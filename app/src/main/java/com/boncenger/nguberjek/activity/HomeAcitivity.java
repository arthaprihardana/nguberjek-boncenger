package com.boncenger.nguberjek.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.AvoidType;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.SearchActivity;
import com.boncenger.nguberjek.app.SharedPreference;
import com.boncenger.nguberjek.fragment.AutoCompleteDestination;
import com.boncenger.nguberjek.fragment.AutoCompletePickUp;
import com.boncenger.nguberjek.model.OrderModel;
import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.MenuAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.app.NavigationDrawer;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.ConvertRupiah;
import com.boncenger.nguberjek.helper.GPSTracker;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.RiderModel;
import com.boncenger.nguberjek.model.RuteModel;
import com.boncenger.nguberjek.model.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sragen on 8/10/2016.
 */
public class HomeAcitivity extends BaseActivity implements DirectionCallback, OnMapReadyCallback, LocationListener {

    Toolbar toolbar;
    DrawerLayout drawer;

    public static boolean mMapIsTouched = false;
    private SessionManager session;
    private HashMap<String, String> user;

    private ImageView imgProfile;
    private TextView tvUser;
    private TextView tvPhone;

    private ProgressDialog dialog;
    // maps //
    private MapView mv;
    private GoogleMap map;
    private UiSettings mUiSettings;
    private LocationManager locationManager;
    private String locationProvider;
    private static LatLng CURRENT_lOCATION;
    private static LatLng DESTINATION_lOCATION;
    private static LatLng TEMP_lOCATION;
    private static LatLng LOCATION_BEFORE;

    private double latitude, longitude;
    private String currentAddress = "";
    private double latitudeDes, longitudeDes;
    private String currentAddressDes = "";
    private GPSTracker gps;

    // navigation //
    private RelativeLayout btnNguberJek;
    private RelativeLayout btnNguberTaxi;
    private RelativeLayout btnNguberCar;
    private ImageView imgNguberJek;
    private ImageView imgNguberTaxi;
    private ImageView imgNguberCar;
    private FrameLayout lineNguberJek;
    private FrameLayout lineNguberTaxi;
    private FrameLayout lineNguberCar;
    private RelativeLayout ketMarker;
    private RelativeLayout imgMarker;
    private TextView tvHarga, tvJarak, tvWaktu;

    // status //
    private String service_status = "nguberjek";

    // rider nearby //
    private ArrayList<RiderModel> riderList = new ArrayList<RiderModel>();
    private ArrayList<LatLng> locationList = new ArrayList<LatLng>();

    // auto complete //
    ArrayList resultId = null;
    private static final String LOG_TAG = "Google Places Autocomplete";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";//
    private static final String OUT_JSON = "/json";

//    private static final String API_KEY = "AIzaSyCbj0WG88GvLFjyxNTYBpXGvaO9y_AK6U0" // DEVELOPMENT
//    private static final String API_KEY = "AIzaSyDnTce5NZQrxVHh6r7hLkAdeTF9VU8Adz4"; // PRODUCTION
    private static final String API_KEY = "AIzaSyBvgQv-M7of7W4IN-vlxXYW6QJHzQxx9Iw"; // PRODUCTION


    /// STATUS APPLICATION ///
    private String TOKEN;
    private boolean getLocation = true;
    private boolean isLocationAwal = true;
    String price, distance, duration;
    private String orderId = "";
    private boolean isFirstTime = true;
    private boolean findLocation = true;


    /// content for loading ////

    private RelativeLayout rlMain, rlLoading;
    private ImageView imageLoading;

    private SmoothProgressBar progressIndicator;


    // SOCKET APP //
    private Socket mSocket;

    // autocomplete //
    EditText placeAutoComplete_pickUp;
    EditText placeAutoComplete_destination;
    private String pickUp_address;
    private String destination_address;
    private GoogleApiClient mGoogleApiClient;

    public static int result_search_pickup=145;
    public static int result_search_destination=245;

    public static boolean isResetData=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        /// SESSION ///
        session = new SessionManager(this);
        user = session.getUserDetails();
        log.logD(user.get(SessionManager.KEY_MEMBER_TOKEN));
        ///////////////

        /// SOCKET ///
        AppController app = (AppController) getApplication();
        mSocket = app.getSocket();
        //////////////


        isResetData=false;

        setToolBar();
        setNavigationDrawer();
        setContent();
        getCurrentLocation_FirstTime();


        //startActivity(new Intent(HomeAcitivity.this,UiSettingsDemoActivity.class));


        // GET BUNDLE FROM OTHER PAGE //
        try {
            // SETELAH REGISTER //
            Bundle b = getIntent().getExtras();
            if (b.getBoolean("fromRegister"))
                dialog_message2("TERIMAKASIH", "Selamat menggunakan NGUBERJEK");


            // DATANG DARI PAGE FAVORITE //
            RuteModel ruteModel = (RuteModel) b.getSerializable("ruteModel");
            if (ruteModel != null) {
                isLocationAwal = false;
                ketMarker.setVisibility(View.GONE);
                imgMarker.setVisibility(View.GONE);

                CURRENT_lOCATION = new LatLng(ruteModel.awalLat, ruteModel.awalLng);
                DESTINATION_lOCATION = new LatLng(ruteModel.tujuanLat, ruteModel.tujuanLng);
                placeAutoComplete_pickUp.setText(ruteModel.lokasiAwal);
                placeAutoComplete_destination.setText(ruteModel.lokasiAkhir);


                new CountDownTimer(2000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        Log.d("REQUEST DIRECTION","DARI ONCREATE");
                        requestDirection();
                    }
                }.start();

            }
        } catch (Throwable throwable) {
        }

        try {
            Bundle b = getIntent().getExtras();
            if (b.getString("message") != null)
                dialog_message2("", b.getString("message"));
        } catch (Throwable throwable) {
        }


        try {


            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.framentMap);
            mapFragment.getMapAsync(this);



        } catch (Throwable tr) {}

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==result_search_pickup){
            if(resultCode == Activity.RESULT_OK) {
                Double lat = data.getDoubleExtra("lat", 0);
                Double lng = data.getDoubleExtra("lng", 0);
                String address = data.getStringExtra("address");
                String name = data.getStringExtra("name");

                isLocationAwal = true;
                findLocation=false;

                placeAutoComplete_pickUp.setText(name);
                pickUp_address = address;
                CURRENT_lOCATION = new LatLng(lat, lng);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 15));
                getRiderNear();
                map.clear();
                map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));
            }else if(resultCode==Activity.RESULT_CANCELED){
                isLocationAwal=true;
                findLocation=true;

                ketMarker.setVisibility(View.VISIBLE);
                imgMarker.setVisibility(View.VISIBLE);

            }
        }else if(requestCode==result_search_destination){
            if(resultCode == Activity.RESULT_OK) {
                Double lat = data.getDoubleExtra("lat", 0);
                Double lng = data.getDoubleExtra("lng", 0);
                String address = data.getStringExtra("address");
                String name = data.getStringExtra("name");

                isLocationAwal = false;
                findLocation=false;

                placeAutoComplete_destination.setText(name);
                destination_address=address;
                DESTINATION_lOCATION = new LatLng(lat,lng);
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(DESTINATION_lOCATION, 15));

                map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));


                Log.d("REQUEST DIRECTION","DARI GET LAT LNG AUTOCOMPLETE");
                requestDirection();
            }else if(resultCode==Activity.RESULT_CANCELED){
                isLocationAwal=false;
                findLocation=true;

                ketMarker.setVisibility(View.VISIBLE);
                imgMarker.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (isMapPermissionGranted()) setMap();

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);
        if (this.map != null) {
            this.initializeMap();
        }
        //initialize the locaiton manager
        this.initializeLocationManager();
        this.locationManager.requestLocationUpdates(this.locationProvider, 400, 1, this);
    }






    private void setMap() {
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (findLocation) {
                    ketMarker.setVisibility(View.VISIBLE);
                    imgMarker.setVisibility(View.VISIBLE);
                }
            }
        });
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions
                getCurrentLocation();
                return false;
            }
        });
        ImageView current = (ImageView) findViewById(R.id.imgCurrentLocation);
        current.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentLocation_FirstTime();
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 15));
                map.animateCamera(CameraUpdateFactory.zoomTo(15), 3000, null);
            }
        });

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 15));
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);

//        final LocationAddress locationAddress = new LocationAddress();
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                if (findLocation) {
                    // change status for keterangan marker //
                    ketMarker.setVisibility(View.VISIBLE);
                    imgMarker.setVisibility(View.VISIBLE);
                }else{
                    // change status for keterangan marker //
                    ketMarker.setVisibility(View.GONE);
                    imgMarker.setVisibility(View.GONE);
                }

                TEMP_lOCATION = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
                if (getLocation) {
                    /*locationAddress.getAddressFromLocation(cameraPosition.target.latitude, cameraPosition.target.longitude,
                            getApplicationContext(), new GeocoderHandler());*/

                    getGeoLocation(cameraPosition.target.latitude,cameraPosition.target.longitude);
                }

                hideSoftKeyboard(HomeAcitivity.this);
            }
        });


        setNavigation();
        setService();
    }


    @Override
    protected void onPause() {
        super.onPause();

        socketOff();

    }


    private void setContent() {
        progressIndicator = (SmoothProgressBar) findViewById(R.id.progress);
        // search location //
        placeAutoComplete_pickUp=(EditText)findViewById(R.id.lokasi_awal);
        placeAutoComplete_destination=(EditText)findViewById(R.id.lokasi_tujuan);
        // intialized services //
        btnNguberJek = (RelativeLayout) findViewById(R.id.btnNguberJek);
        btnNguberTaxi = (RelativeLayout) findViewById(R.id.btnNguberTaxi);
        btnNguberCar = (RelativeLayout) findViewById(R.id.btnNguberCar);
        imgNguberJek = (ImageView) findViewById(R.id.imgNguberJek);
        imgNguberTaxi = (ImageView) findViewById(R.id.imgNguberTaxi);
        imgNguberCar = (ImageView) findViewById(R.id.imgNguberCar);
        lineNguberJek = (FrameLayout) findViewById(R.id.lineNguberjek);
        lineNguberTaxi = (FrameLayout) findViewById(R.id.lineNguberTaxi);
        lineNguberCar = (FrameLayout) findViewById(R.id.lineNguberCar);
        ketMarker = (RelativeLayout) findViewById(R.id.keteranganMarker);
        imgMarker = (RelativeLayout) findViewById(R.id.imgMarker);
        tvHarga = (TextView) findViewById(R.id.tvHarga);
        tvJarak = (TextView) findViewById(R.id.tvJarak);
        tvWaktu = (TextView) findViewById(R.id.tvWaktu);

        ketMarker.setVisibility(View.GONE);
        rlMain = (RelativeLayout) findViewById(R.id.containerMain);
        rlLoading = (RelativeLayout) findViewById(R.id.containerLoading);
        imageLoading = (ImageView) findViewById(R.id.imageLoading);
        Glide.with(this)
                .load(R.drawable.loading)
                .placeholder(R.drawable.loading_placeholder)
                //.asGif()
                .crossFade()
                .into(imageLoading);
        rlMain.setVisibility(View.VISIBLE);
        rlLoading.setVisibility(View.GONE);
    }

    private void setService() {
        imgNguberJek.setImageResource(R.mipmap.icon_ojek02);
        imgNguberTaxi.setImageResource(R.drawable.restaurant);
        imgNguberCar.setImageResource(R.mipmap.car02);
        lineNguberJek.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        lineNguberTaxi.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        lineNguberCar.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        switch (service_status) {
            case "nguberjek":
                imgNguberJek.setImageResource(R.mipmap.icon_ojek);
                lineNguberJek.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
            case "ngubertaxi":
//                imgNguberTaxi.setImageResource(R.mipmap.taxi01);
                imgNguberTaxi.setImageResource(R.drawable.nguberfood);
                lineNguberTaxi.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
            case "ngubercar":
                imgNguberCar.setImageResource(R.mipmap.car01);
                lineNguberCar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
                break;
        }
        getRiderNear();
//        emit_RiderNear();
    }

    private void setNavigation() {
        findViewById(R.id.imgTo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        placeAutoComplete_pickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeAcitivity.this, SearchActivity.class);
                startActivityForResult(intent, result_search_pickup);
            }
        });
        placeAutoComplete_destination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(HomeAcitivity.this, SearchActivity.class);
                startActivityForResult(intent, result_search_destination);
            }
        });

        btnNguberJek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service_status = "nguberjek";
                setService();
            }
        });
        btnNguberTaxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //service_status = "ngubertaxi";
                //setService();

                Intent intent = new Intent(HomeAcitivity.this, BookingActivity.class);
                Bundle b=new Bundle();
                b.putString("status","2");
                b.putString("url","https://goo.gl/forms/ZDZHWjM4b2pRUBug2");
                intent.putExtras(b);
                startActivity(intent);


                //startActivity(new Intent(HomeAcitivity.this, NguberFoodActivity.class));
            }
        });
        btnNguberCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                service_status = "ngubercar";
                setService();
            }
        });
        findViewById(R.id.btnOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareOrder();
            }
        });
        ketMarker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ketMarker.setVisibility(View.GONE);
                imgMarker.setVisibility(View.GONE);

                /*final LocationAddress locationAddress = new LocationAddress();
                locationAddress.getAddressFromLocation(TEMP_lOCATION.latitude, TEMP_lOCATION.longitude,
                        getApplicationContext(), new GeocoderHandler());*/
                getGeoLocation(TEMP_lOCATION.latitude,TEMP_lOCATION.longitude);
                hideSoftKeyboard(HomeAcitivity.this);
            }
        });

        findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_cancel("", "Anda yakin ingin membatalkan order ?");
            }
        });
    }

    private void prepareOrder() {
        boolean canOrder = true;
        String msg = "";
        if (pickUp_address == null) {
            canOrder = false;
            msg = "Silahkan masukkan lokasi awal";
        }
        if (destination_address == null ) {
            canOrder = false;
            msg = "Silahkan masukkan lokasi tujuan";
        }
        if (distance == null) {
            canOrder = false;
            msg = "Jarak belum ditentukan";
        }
        if (duration == null) {
            canOrder = false;
            msg = "Waktu belum ditentukan";
        }
        if (price == null) {
            canOrder = false;
            msg = "Harga belum ditentukan";
        }
        /*if(riderList.size()<1){
            canOrder=false;
            msg="Maaf tidak ada rider disekitar anda";
        }*/


        if (canOrder) {
//            requestOrder();

            OrderModel orderModel = new OrderModel();
            orderModel.order_location_awal = placeAutoComplete_pickUp.getText()+"\n"+pickUp_address;
            orderModel.order_longitude_awal = CURRENT_lOCATION.longitude;
            orderModel.order_latitude_awal = CURRENT_lOCATION.latitude;
            orderModel.order_location_tujuan = placeAutoComplete_destination.getText()+"\n"+destination_address;
            orderModel.order_longitude_tujuan = DESTINATION_lOCATION.longitude;
            orderModel.order_latitude_tujuan = DESTINATION_lOCATION.latitude;
            orderModel.order_price = price;
            orderModel.order_distance = distance;
            orderModel.order_duration = duration;
            orderModel.order_service = service_status;

            if(riderList.size()>0) {
                Bundle b = new Bundle();
                b.putSerializable("data", orderModel);
                Intent intent = new Intent(HomeAcitivity.this, WaitingActivity.class);
                intent.putExtras(b);
                startActivity(intent);
            }else{
                Toast.makeText(getApplicationContext(),"Tidak ada rider di area Anda",Toast.LENGTH_LONG).show();
            }
        } else {
            dialog_message(msg);
        }
    }




    public void getGeoLocation(final double latitude , final double longitude) {
        String sb = "https://maps.googleapis.com/maps/api/geocode/json";
        sb += "?latlng=" +latitude +","+longitude;
        sb += "&key=" + API_KEY;
        Log.d("url geolocation", sb.toString());
        String tag_json_obj = "geolocation";

        setService();
        progressIndicator.setVisibility(View.VISIBLE);
        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, sb, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressIndicator.setVisibility(View.GONE);
                    String formatted_address="";
                    Log.d("output response", response.toString());
                    JSONArray jsonArray=response.getJSONArray("results");
                    if(jsonArray.length()>0){
                        JSONObject data=jsonArray.getJSONObject(0);
                        formatted_address=data.getString("formatted_address");
                    }








                    if (formatted_address.toLowerCase().startsWith("latitude:")) {
                        dialog_message("Maaf koneksi anda terputus, silahkan coba lagi");
                    }else {
                        String[] address_split = formatted_address.split(",");
                        currentAddress = address_split[0];
                        getLocation = false;




                        if (isFirstTime) {
                            isFirstTime = false;
                            placeAutoComplete_pickUp.setText(currentAddress);
                            pickUp_address=formatted_address;
                        } else {
                            map.clear();
                            setRiderMarker();
                            if (isLocationAwal) {

                                placeAutoComplete_pickUp.setText(currentAddress);
                                pickUp_address=formatted_address;

                                CURRENT_lOCATION = new LatLng(latitude, longitude);
                                map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));



                                placeAutoComplete_destination.setText("");
                                DESTINATION_lOCATION = new LatLng(0, 0);
                                tvHarga.setText("Harga : ");
                                tvJarak.setText("Jarak : ");
                                tvWaktu.setText("Waktu : ");

                                price = null;
                                distance = null;
                                duration = null;
                            } else {

                                placeAutoComplete_destination.setText(currentAddress);
                                destination_address=formatted_address;

                                DESTINATION_lOCATION = new LatLng(latitude, longitude);
                                map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));

                                // request direction //
                                map.clear();
                                setRiderMarker();
                                Log.d("REQUEST DIRECTION","DARI LOCATION ADDRESS NAME");
                                requestDirection();
                            }
                        }

                    }





















                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "gagal geolocation", Toast.LENGTH_SHORT).show();
                    Log.d("Geolocation", "Error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Maaf koneksi anda terputus, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                VolleyLog.d("get lat", "Error: " + error.getMessage());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();


                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }





    /**
     *
     * @param input
     * @return
     */
    public ArrayList autocomplete(String input) {
        ArrayList resultList = null;
        resultId = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:id");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            Log.d("url", sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.d(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.d(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            Log.d("result", jsonObj.toString());

            // Extract the Place descriptions from the results
            resultList = new ArrayList(predsJsonArray.length());
            resultId = new ArrayList(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                /*boolean is_geocode=false;
                JSONArray types_array=predsJsonArray.getJSONObject(i).getJSONArray("types");
                for(int x=0;x<types_array.length();x++){
                    //System.out.println(types_array.get(x));
                    if(types_array.get(x).equals("geocode"))is_geocode=true;
                    else  is_geocode=false;
                }

                if(is_geocode) {
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    System.out.println("============================================================");
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                    resultId.add(predsJsonArray.getJSONObject(i).getString("place_id"));
                }*/

                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
                resultId.add(predsJsonArray.getJSONObject(i).getString("place_id"));
            }


        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }
        return resultList;
    }


    /**
     *
     */
    public void getCurrentLocation() {
        LatLng my_coordinate;
        try {
            gps = new GPSTracker(this);
            if (gps.canGetLocation()) { // gps enabled
            } else {
                gps.showSettingsAlert();
            }
            my_coordinate = (LatLng) SharedPreference.Get(context, context.getString(R.string.my_coordinate), LatLng.class);
            if (my_coordinate == null) {
                my_coordinate = new LatLng(-6.170933, 106.826830);
            }
        }catch (Throwable throwable){
            my_coordinate = new LatLng(-6.170933, 106.826830);
        }

        CURRENT_lOCATION= my_coordinate;
    }

    public void getCurrentLocation_FirstTime(){
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) { // gps enabled
            CURRENT_lOCATION = new LatLng(gps.getLatitude(), gps.getLongitude());
            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            CURRENT_lOCATION = new LatLng(-6.1756484, 106.8227754);
            CURRENT_lOCATION = new LatLng(0, 0);
            //gps.stopUsingGPS();
        }
    }

    public void dialog_message(String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("");
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.show();
        } catch (Throwable throwable) {
        }
    }

    public void dialog_message2(String title, String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.show();
        } catch (Throwable throwable) {
        }
    }


    public void dialog_cancel(String title, String message) {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!orderId.equals("")) cancelRequest();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        } catch (Throwable throwable) {
        }
    }

    private void showUpdateApps() {
        try {
            Date Date1 = new Date();
            Date Date2 = (Date) SharedPreference.Get(this, getString(R.string.timeUpdate), Date.class);
            boolean isShow = false;

            if (Date2 != null) {
                SimpleDateFormat format = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy");
                //Date2 = format.parse("Fri Nov 18 10:05:05 2016");
                Log.d(HomeAcitivity.class.getSimpleName(), Date1.toString());
                Log.d(HomeAcitivity.class.getSimpleName(), Date2.toString());

                long mills = Date1.getTime() - Date2.getTime();
                int Hours = (int) (mills / (1000 * 60 * 60));
                int Mins = (int) (mills % (1000 * 60 * 60));
                String diff = Hours + ":" + Mins;

                Log.d(HomeAcitivity.class.getSimpleName(), Hours + "");
                if (Hours > 0) {
                    isShow = true;
                }
            } else {
                isShow = true;
            }


            if (isShow) {
                /// CEKING IS UPDATE FROM API ///
                Log.e("is update", getUpdateApp() + "");
                if (getUpdateApp()) {
                    dialog_update();
                    SharedPreference.Save(this, getString(R.string.timeUpdate), Date1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setUpdateApp(Boolean updateApp) {
        SharedPreference.Save(getContext(), "isUpdate", updateApp);
    }

    public static boolean getUpdateApp() {
        Boolean value = (Boolean) SharedPreference.Get(getContext(), "isUpdate", Boolean.class);
        return (value != null && (boolean) value);
    }

    public void dialog_update() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("");
        builder.setMessage("Aplikasi Nguberjek sudah ada versi terbaru. Silahkan update versi terbaru sekarang!");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                goPlayStore();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void goPlayStore() {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }


    private void socketOn() {
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("server-response: radius driver", onRiderNear);
        mSocket.connect();
    }

    private void socketOff() {
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("server-response: radius driver", onRiderNear);
    }


    private void emit_RiderNear() {
        socketOff();
        socketOn();
        JSONObject data = new JSONObject();
        try {
            data.put("token", user.get(SessionManager.KEY_MEMBER_TOKEN));
            data.put("koordinat", CURRENT_lOCATION.longitude + "," + CURRENT_lOCATION.latitude);
            data.put("role", service_status);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("client-request: radius driver", data);
        Log.d("SOCKET", "client-request: radius driver");
        Log.d("SOCKET", data.toString());
        progressIndicator.setVisibility(View.VISIBLE);
    }


    private Emitter.Listener onRiderNear = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        progressIndicator.setVisibility(View.GONE);
                        JSONObject response = new JSONObject(args[0].toString());
                        Log.d("SOCKET", "server-response: radius driver");
                        Log.d("SOCKET", response.toString());

                        String status = response.getString("s");
                        String statusCode = response.getString("statusCode");

                        if (status.equals("1")) {
                            riderList = new ArrayList<RiderModel>();
                            if (!response.isNull("dt")) {
                                JSONArray dataArray = response.getJSONArray("dt");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject data = dataArray.getJSONObject(i);
                                    riderList.add(new RiderModel(data));
                                }
                            }
                            map.clear();
                            setRiderMarker();
                        } else {
                            if (statusCode.equals("400")) {
                                goLogin();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        return;
                    }


                }
            });
        }
    };


    private void getRiderNear() {

        if (isOnline()) {
            map.clear();
            final String url = AppConstants.APIRiderNear + "?koordinat=" + CURRENT_lOCATION.longitude + "," + CURRENT_lOCATION.latitude
                    + "&role=" + service_status;
//                    + "&limit=10&page=1";
            final String url2 = AppConstants.APIRiderNear + "?koordinat=106.974478,-6.277156"
                    + "&role=" + service_status;
//                    + "&limit=10&page=1";
            log.logD(url);
            String tag_json_obj = "getNear";
            progressIndicator.setVisibility(View.VISIBLE);


            // creating request obj
            CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        log.logD(response.toString());
                        String status = response.getString("s");

                        if (status.equals("1")) {
                            //int versionCodeServer = response.getInt("versionCodeBoncenger");
                            //setUpdateApp(checkVersionCode(versionCodeServer));

                            riderList = new ArrayList<RiderModel>();
                            JSONArray dataArray = response.getJSONArray("dt");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject data = dataArray.getJSONObject(i);
                                riderList.add(new RiderModel(data));
                            }
                            setRiderMarker();

                        }
                        progressIndicator.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(),
                                e.toString(),
                                Toast.LENGTH_LONG).show();
                        progressIndicator.setVisibility(View.GONE);


                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                /*Toast.makeText(getActivity(),
                        "Please check your internet connection !" ,
                        Toast.LENGTH_LONG).show();*/
                    progressIndicator.setVisibility(View.GONE);
                    //goLogin();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("x-access-token", user.get(SessionManager.KEY_MEMBER_TOKEN));
                    params.put("X-OS-Version", getSDKVersion() + "");
                    params.put("X-Client-Version", getAppBuild() + "");
                    log.logD(params.toString());
                    return params;
                }

            };
            AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        } else {
            dialog_message("Maaf koneksi anda terputus, silahkan coba lagi");
        }
    }

    public static int getSDKVersion() {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static int getAppBuild() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return 0;
    }


    private void cancelRequest() {
        final String url = AppConstants.APiCancelRequest;
        log.logD(url);
        String tag_json_obj = "cancel_request";
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status = response.getString("s");
                    String message = response.getString("msg");
                    if (status.equals("1")) {
                        rlLoading.setVisibility(View.GONE);
                        rlMain.setVisibility(View.VISIBLE);
                    }

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString(),
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                goLogin();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_MEMBER_TOKEN));
                log.logD(params.toString());
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId", orderId);

                log.logD(params.toString());
                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void getFare(final String distancee_value,final String distancee_text, final String durationn) {
        final String url = AppConstants.APIFare + "?jarak=" + distancee_value
                + "&role=" + service_status;
        log.logD(url);
        String tag_json_obj = "getFare";
        //final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading", true);
        progressIndicator.setVisibility(View.VISIBLE);

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status = response.getString("s");
                    String message = response.getString("msg");
                    if (status.equals("1")) {
                        String pricee = response.getString("dt");
//                        String pricee = response.getString("price");

                        ConvertRupiah rp = new ConvertRupiah();
                        tvHarga.setText("Harga : Rp. " + rp.ConvertRupiah(pricee));
                        tvJarak.setText("Jarak : " + distancee_text );
                        tvWaktu.setText("Waktu : " + durationn);

                        price = pricee;
                        distance = distancee_value;
                        duration = durationn;
                    } else {
                        dialog_message(message);
                        tvHarga.setText("Harga : ");

                        price = null;
                    }
                    progressIndicator.setVisibility(View.GONE);
                    //dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString(),
                            Toast.LENGTH_LONG).show();
                    progressIndicator.setVisibility(View.GONE);
                    //dialog.dismiss();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                //dialog.dismiss();
                progressIndicator.setVisibility(View.GONE);
                goLogin();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("x-access-token", user.get(SessionManager.KEY_MEMBER_TOKEN));
                log.logD(params.toString());
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }




    private void setRiderMarker() {
        LatLng LOCATION;
        for (int i = 0; i < riderList.size(); i++) {
            LOCATION = new LatLng(riderList.get(i).rider_lat, riderList.get(i).rider_lng);
            int markerType = 0;
            if (service_status.equals("nguberjek")) {
                markerType = R.mipmap.rider;
            } else if (service_status.equals("ngubertaxi")) {
                markerType = R.mipmap.driver_taxi;
            } else if (service_status.equals("ngubercar")) {
                markerType = R.mipmap.driver_car;
            }
            Marker marker = map.addMarker(new MarkerOptions()
                    .position(LOCATION)
                    .icon(BitmapDescriptorFactory.fromResource(markerType))
            );
            marker.showInfoWindow();
        }
        if (distance != null) {
            if (!findLocation) {
                Log.d("REQUEST DIRECTION","DARI SET RIDER MARKER");
                requestDirection();
            }
        }
    }



    public class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {
        private ArrayList resultList;
        LayoutInflater inflater;

        public GooglePlacesAutocompleteAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            String result = "";
            try {
                result = resultList.get(index).toString();
            } catch (Throwable th) {
            }
            return result;
        }

        @Override
        public long getItemId(int position) {
            return super.getItemId(position);
        }


        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }


    }


    public void requestDirection() {
        log.logD(CURRENT_lOCATION.toString() + "======" + DESTINATION_lOCATION.toString());
        if(service_status.equals("nguberjek")){
            GoogleDirection.withServerKey(API_KEY)
                    .from(CURRENT_lOCATION)
                    .to(DESTINATION_lOCATION)
                    .transportMode(TransportMode.DRIVING)
                    .avoid(AvoidType.TOLLS)
                    .execute(this);
        }else{
            GoogleDirection.withServerKey(API_KEY)
                    .from(CURRENT_lOCATION)
                    .to(DESTINATION_lOCATION)
                    .transportMode(TransportMode.DRIVING)
                    .execute(this);
        }


    }

    /**
     *
     * @param direction
     * @param rawBody
     */
    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {
            map.clear();
            // disable all status //
            findLocation = false;
            imgMarker.setVisibility(View.GONE);
            ketMarker.setVisibility(View.GONE);
            ////////////////////////


            map.addMarker(new MarkerOptions().position(CURRENT_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_awal)));
            map.addMarker(new MarkerOptions().position(DESTINATION_lOCATION).icon(BitmapDescriptorFactory.fromResource(R.drawable.lokasi_tujuan)));

            // get distance  and  duration//
            String distance_text = direction.getRouteList().get(0).getLegList().get(0).getDistance().getText();
            String distance_value = direction.getRouteList().get(0).getLegList().get(0).getDistance().getValue();

            String duration = direction.getRouteList().get(0).getLegList().get(0).getDuration().getText();

            double distance_=0;
            try {
                distance_=Double.parseDouble(distance_value);
                distance_= distance_/1000;
            } catch (Throwable th) {
            }

            distance=distance_+"";
            getFare(distance_+"", distance_text, duration);

            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.RED));
            LatLngBounds.Builder BOUNDS_CAMERA = new LatLngBounds.Builder();
            BOUNDS_CAMERA.include(DESTINATION_lOCATION).include(CURRENT_lOCATION);
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(BOUNDS_CAMERA.build(), 150));

        } else {
            Toast.makeText(getApplicationContext(), "direction gagal, silahkan coba lagi", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.d("direction", t.getMessage());
        //Snackbar.make(btnRequestDirection, t.getMessage(), Snackbar.LENGTH_SHORT).show();
    }




    private void goLogin(){
        final String url = AppConstants.APILogin;
        String tag_json_obj = "Login";

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status = response.getString("s");
                    String message = response.getString("msg");
                    TOKEN = response.getString("token");

                    if (status.equals("1")) {
                        JSONObject data = response.getJSONObject("dt");


                        Toast.makeText(getApplicationContext(), "Auto get Token ", Toast.LENGTH_SHORT).show();
                        createSession(data);
                        startActivity(new Intent(HomeAcitivity.this, HomeAcitivity.class));
                        finish();
                        ActivityManager.getInstance().finishAll();
                    } else {
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please check your internet connection !",
                            Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", user.get(SessionManager.KEY_MEMBER_USERNAME));
                params.put("password", user.get(SessionManager.KEY_MEMBER_PASSWORD));
                params.put("senderId", user.get(SessionManager.KEY_SENDER_ID));
                log.logD(params.toString());
                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void createSession(JSONObject data) throws JSONException {
        String id = data.getString("_id");
        String username = data.getString("username");
        String nama = data.getString("nama");
        String email = data.getString("email");
        String phone = data.getString("noTelp");
        String gender = data.getString("jenisKelamin");
        String image = data.getString("photo");

        Session sessionModel = new Session();
        sessionModel.setMember_id(id);
        sessionModel.setMember_username(username);
        sessionModel.setMember_password(user.get(SessionManager.KEY_MEMBER_PASSWORD));
        sessionModel.setMember_firstname(nama);
        sessionModel.setMember_email(email);
        sessionModel.setMember_phone(phone);
        sessionModel.setMember_gender(gender);
        sessionModel.setMember_token(TOKEN);
        sessionModel.setMember_foto(image);
        sessionModel.setSender_id(user.get(SessionManager.KEY_SENDER_ID));
        ArrayList<Session> sessionList = new ArrayList<Session>();
        sessionList.add(sessionModel);

        session.logoutUser();
        session.createLoginSession(sessionList);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        rlLoading.setVisibility(View.GONE);
        rlMain.setVisibility(View.VISIBLE);

        socketOff();
    }

    private String TAG = "PERMISSION";

    public boolean isMapPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            getCurrentLocation_FirstTime();
            setMap();
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }


    private void setToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
           /* actionBar.setHomeAsUpIndicator(R.drawable.back);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
*/
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo = (ImageView) mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle = (TextView) mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("NGUBERJEK");


            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showDataProfile();
        showUpdateApps();

        socketOn();
        Log.i("called", "Activity --> onResume");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //this.locationManager.requestLocationUpdates(this.locationProvider, 400, 1, this);

        version();

        /*// reset all data //
        if(isResetData) {
            map.clear();
            isFirstTime=true;

            pickUp_address = "";
            destination_address = "";
            placeAutoComplete_pickUp.setText("");
            placeAutoComplete_destination.setText("");
            CURRENT_lOCATION = new LatLng(0, 0);
            DESTINATION_lOCATION = new LatLng(0, 0);
            tvHarga.setText("Harga : ");
            tvJarak.setText("Jarak : ");
            tvWaktu.setText("Waktu : ");

            price = null;
            distance = null;
            duration = null;

            isResetData=false;
        }*/


    }

    private void showDataProfile(){
        session=new SessionManager(this);
        user=session.getUserDetails();
        Glide.with(this)
                .load(user.get(SessionManager.KEY_MEMBER_IMAGE))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imgProfile);
        tvUser.setText(user.get(SessionManager.KEY_MEMBER_FIRSTNAME));
        tvPhone.setText(user.get(SessionManager.KEY_MEMBER_PHONE));
    }

    private void setNavigationDrawer(){
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        View headerView = View.inflate(HomeAcitivity.this, R.layout.nav_header_main, null);
        ListView listView=(ListView)findViewById(R.id.listView);
        imgProfile=(ImageView)headerView.findViewById(R.id.circleView);
        tvUser=(TextView)headerView.findViewById(R.id.tvName);
        tvPhone=(TextView)headerView.findViewById(R.id.tvPhone);
        headerView.findViewById(R.id.imgPencil).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAcitivity.this, EditProfileActivity.class));
            }
        });
        findViewById(R.id.tvTentangKami).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeAcitivity.this, TentangKamiActivity.class));
            }
        });
        findViewById(R.id.tvLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
                dialog_logout();
            }
        });

        listView.addHeaderView(headerView,null, false);
        MenuAdapter menuAdapter=new MenuAdapter(HomeAcitivity.this);
        listView.setAdapter(menuAdapter);

        Glide.with(this)
                .load(user.get(SessionManager.KEY_MEMBER_IMAGE))
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imgProfile);
        tvUser.setText(user.get(SessionManager.KEY_MEMBER_FIRSTNAME));
        tvPhone.setText(user.get(SessionManager.KEY_MEMBER_PHONE));


        NavigationDrawer drawerClick=new NavigationDrawer(this,listView,drawer);
    }

    private void dialog_logout(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Apakah anda yakin ingin keluar ? ");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                session.logoutUser();
                doLogout();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }


    private void doLogout(){
        final String url= AppConstants.APILogout+"?_id="+user.get(SessionManager.KEY_MEMBER_ID);
        String tag_json_obj="Logout";
        Log.d("url","logout "+url);

        final ProgressDialog dialog= ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        session.logoutUser();
                        finish();
                        ActivityManager.getInstance().finishAll();
                        startActivity(new Intent(HomeAcitivity.this, IntroPageActivity.class));
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please check your internet connection !" ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                goLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Access-Token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }



    //----------------------------------------
    //	Summary: For initializing the map
    //----------------------------------------
    private void initializeMap() {

        //set map type
        this.map.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //TODO: other map initialization as needed
    }

    //-------------------------------------------
    //	Summary: initialize location manager
    //-------------------------------------------
    private void initializeLocationManager() {

        //get the location manager
        this.locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        //define the location manager criteria
        Criteria criteria = new Criteria();

        this.locationProvider = locationManager.getBestProvider(criteria, false);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(locationProvider);


        //initialize the location
        if(location != null) {

            onLocationChanged(location);
        }
    }


    //------------------------------------------
    //	Summary: Location Listener  methods
    //------------------------------------------
    @Override
    public void onLocationChanged(Location location) {

        Log.i("called", "onLocationChanged");

        try {

            //when the location changes, update the map by zooming to the location
            LatLng my_position = new LatLng(location.getLatitude(), location.getLongitude());
            Log.i("called","Update map");
            SharedPreference.Save(HomeAcitivity.this, getString(R.string.my_coordinate), my_position);


        }catch (Throwable throwable){}
    }

    @Override
    public void onProviderDisabled(String arg0) {

        Log.i("called", "onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String arg0) {

        Log.i("called", "onProviderEnabled");
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

        Log.i("called", "onStatusChanged");
    }


    public void version() {
        String url = AppConstants.APIVersion ;
        Log.d("url",url);
        String tag_json_obj = "ping";

        // creating request obj
        final CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d(TAG, response.toString());

                    if(!response.isNull("data")){
                        JSONObject data=response.getJSONObject("data");
                        String versionRider=data.getString("riderVersion");
                        String versionBoncenger=data.getString("boncegerVersion");

                        int versionCodeServer = Integer.parseInt(versionBoncenger);
                        setUpdateApp(checkVersionCode(versionCodeServer));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                Log.d(TAG, params.toString());

                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d(TAG, params.toString());

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

}
