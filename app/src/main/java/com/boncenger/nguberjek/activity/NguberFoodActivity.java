package com.boncenger.nguberjek.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.KategoriFoodAdapter;
import com.boncenger.nguberjek.app.API;
import com.boncenger.nguberjek.app.ErrorCallback;
import com.boncenger.nguberjek.app.ResponseCallback;
import com.boncenger.nguberjek.model.CategoryModel;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by adikurniawan on 10/7/17.
 */

public class NguberFoodActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private GridLayoutManager lLayout;
    private ProgressBar progressBar;
    private RecyclerView rView;
    private ScrollView scrollView;


    private ArrayList<CategoryModel> rowList=new ArrayList<CategoryModel>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nguberfood);

        setToolbar();
        setContent();
        setNavigation();
        getCategoryList("");
    }

    private void setContent(){
        scrollView=(ScrollView)findViewById(R.id.scrollView);
        ImageView imgBanner=(ImageView)findViewById(R.id.imgBanner);
        ImageView imgPromo=(ImageView)findViewById(R.id.imgPromo);
        ImageView imgNear=(ImageView)findViewById(R.id.imgNear);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        rView = (RecyclerView)findViewById(R.id.recyclerView);
        Glide.with(this)
                .load(R.drawable.banner_nguberfood)
                .into(imgBanner);
        Glide.with(this)
                .load(R.drawable.promotion)
                .into(imgPromo);
        Glide.with(this)
                .load(R.drawable.near_me)
                .into(imgNear);

        final EditText etSearch=(EditText)findViewById(R.id.et_search);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getCategoryList(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });



    }


    private void setNavigation(){
        findViewById(R.id.imgNear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewResto("Near Me");
            }
        });
        findViewById(R.id.imgPromo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewResto("Promotion");
            }
        });
    }


    private void setData(){
        lLayout = new GridLayoutManager(this, 3);


        rView.setNestedScrollingEnabled(false);
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);


        KategoriFoodAdapter rcAdapter = new KategoriFoodAdapter(this, rowList);
        rView.setAdapter(rcAdapter);

        scrollView.smoothScrollTo(0,0);
    }
    public void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        rView.setVisibility(View.GONE);
    }
    public void hideLoading(){
        progressBar.setVisibility(View.GONE);
        rView.setVisibility(View.VISIBLE);
    }

    public String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            return json;
        } catch (IOException e) {
            return null;
        }
    }

    private void getData(){
        try {
            hideLoading();
            rowList.clear();
            String myJson=inputStreamToString(this.getResources().openRawResource(R.raw.kategori));
            JSONObject data_response = new JSONObject(myJson);
            String status = data_response.getString("s");
            String message = data_response.getString("msg");
            JSONArray data_array = data_response.getJSONArray("dt");

            for(int i=0;i<data_array.length();i++){
                JSONObject data=data_array.getJSONObject(i);
                rowList.add(new CategoryModel(data));
            }
            setData();
        } catch (JSONException e) {
            Log.e("error",e.toString());
        }
    }


    public void getCategoryList(String keyword){
        showLoading();
        API.getCategoryFood(this, keyword, new ResponseCallback() {
           @Override
           public int doAction(String response, int httpCode) {
               try {
                   hideLoading();
                   rowList.clear();
                   JSONObject data_response = new JSONObject(response);
                   String status = data_response.getString("s");
                   String message = data_response.getString("msg");
                   JSONArray data_array = data_response.getJSONArray("dt");

                   for(int i=0;i<data_array.length();i++){
                       JSONObject data=data_array.getJSONObject(i);
                       rowList.add(new CategoryModel(data));
                   }
                   setData();
               } catch (JSONException e) {
                   Log.e("error",e.toString());
               }
               return httpCode;
           }
       },
        new ErrorCallback() {
            @Override
            public void doAction() {
                try {
                    hideLoading();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void viewResto(String title){
        Intent intent=new Intent(this,RestoActivity.class);
        Bundle bundle=new Bundle();
        bundle.putString("title",title);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Nguber Food");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
