package com.boncenger.nguberjek.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.bumptech.glide.Glide;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sragen on 8/20/2016.
 */
public class ReviewActivity extends BaseActivity {
    Toolbar toolbar;

    private OrderModel dataOrder;
    private String order_id;


    private RatingBar ratingBar;
    private EditText etKomentar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        Bundle b=getIntent().getExtras();
        order_id=b.getString("order_id");

        try {
            if (b.getString("message")!=null)
                dialog_message2("", b.getString("message"));
        }catch (Throwable throwable){}

        setToolBar();
        setNavigation();
        setContent();
        getData();

    }


    public void dialog_message2(String title,String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }


    private void setContent(){
        ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        etKomentar=(EditText)findViewById(R.id.etKomentar);
    }

    private void setNavigation(){
        findViewById(R.id.btnFavorit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jadikanFavorit();
            }
        });
        findViewById(R.id.btnSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRating();
            }
        });
    }

    private void setData(){
        ImageView imageRider=(ImageView) findViewById(R.id.imageRider);
        TextView tvName=(TextView)findViewById(R.id.tvName);
        TextView tvNopol=(TextView)findViewById(R.id.tvNopol);

        Glide.with(this)
                .load(dataOrder.rider_photo)
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(imageRider);

        tvName.setText(dataOrder.rider_name);
        tvNopol.setText(dataOrder.rider_nopol);
    }

    private void jadikanFavorit(){
        final String url= AppConstants.APIRiderFavorit;
        log.logD(url);
        String tag_json_obj="riderFavorit";
        final ProgressDialog dialog= ProgressDialog.show(ReviewActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("boncengerId",user.get(SessionManager.KEY_MEMBER_ID));
                params.put("riderId",dataOrder.rider_id);
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void dialog_message_listener(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(ReviewActivity.this,HomeAcitivity.class));
                ActivityManager.getInstance().finishAll();
                finish();
            }
        });
        builder.show();
    }

    private void getData(){
        final String url= AppConstants.APIDetailOrder+"?orderId="+order_id;
        log.logD(url);
        String tag_json_obj="get_data_order";
        final ProgressDialog dialog= ProgressDialog.show(ReviewActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        JSONArray dataArray=response.getJSONArray("dt");
                        if(dataArray.length()>0) {
                            JSONObject data = dataArray.getJSONObject(0);
                            dataOrder = new OrderModel(data);
                            setData();
                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Review Perjalanan Anda");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    private void sendRating(){
        final String url= AppConstants.APISendRating;
        log.logD(url);
        String tag_json_obj="sendRating";
        final ProgressDialog dialog= ProgressDialog.show(ReviewActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.PUT, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        dialog_message_listener(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                final int rate= (int) ratingBar.getRating();

                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId",order_id);
                params.put("rating",rate+"");
                params.put("komentar",etKomentar.getText().toString());
                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}


