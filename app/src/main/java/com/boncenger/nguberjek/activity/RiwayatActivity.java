package com.boncenger.nguberjek.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.adapter.RiwayatAdapter;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.HistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sragen on 8/19/2016.
 */
public class RiwayatActivity extends BaseActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;

    private ArrayList<HistoryModel> historyList=new ArrayList<HistoryModel>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat);

        setToolBar();
        setContent();
        getHistory();
    }

    public void setContent(){
        recyclerView = (RecyclerView) findViewById(R.id.rv);
        progressBar=(ProgressBar) findViewById(R.id.progressBar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    public void setData(){
        RiwayatAdapter adapter = new RiwayatAdapter(this,historyList);
        recyclerView.setAdapter(adapter);
    }

    private void getHistory(){
        final String url= AppConstants.APIHistory+"?boncengerId="+user.get(SessionManager.KEY_MEMBER_ID);
        String tag_json_obj="history";

        final ProgressDialog dialog= ProgressDialog.show(this, "", "Loading", true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        historyList.clear();
                        JSONArray dataArray=response.getJSONArray("dt");
                        for(int i=0;i<dataArray.length();i++){
                            JSONObject data=dataArray.getJSONObject(i);
                            historyList.add(new HistoryModel(data));
                        }
                        setData();
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    dialog.dismiss();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                Toast.makeText(getApplicationContext(),
                        "Please chek your internet connection" ,
                        Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("X-Access-Token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    public void jadikanFav(final String order_id){
        final String url= AppConstants.APIRuteFavorit;
        log.logD(url);
        String tag_json_obj="ruteFav";
        final ProgressDialog dialog= ProgressDialog.show(RiwayatActivity.this, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
                getLogin();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();
                params.put("orderId",order_id);

                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText("Riwayat");

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

