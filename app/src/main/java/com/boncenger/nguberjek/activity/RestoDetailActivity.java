package com.boncenger.nguberjek.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.helper.BaseActivity;
import com.boncenger.nguberjek.helper.GPSTracker;
import com.boncenger.nguberjek.model.RestoModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by adikurniawan on 10/24/17.
 */

public class RestoDetailActivity  extends BaseActivity implements OnMapReadyCallback {
    private RestoModel dataResto;
    private Toolbar toolbar;

    private GoogleMap map;
    private UiSettings mUiSettings;
    private LatLng MY_LOCATION = new LatLng(0, 0);
    private LatLng TEMP_LOCATION = new LatLng(0, 0);
    private LatLng RESTO_LOCATION = new LatLng(0,0);
    private LatLng MONAS = new LatLng(-6.1756484, 106.8227754);
    private GPSTracker gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resto_detail);

        Bundle b=getIntent().getExtras();
        dataResto=(RestoModel)b.getSerializable("data_resto");
        MY_LOCATION=new LatLng(dataResto.lat, dataResto.lng);

        setToolbar();
        setContent();
    }



    private void setContent(){
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.framentMap);
            mapFragment.getMapAsync(this);
        } catch (Throwable tr) {
        }

        TextView tvAddress=(TextView)findViewById(R.id.tvAddress);
        TextView tvPhone=(TextView)findViewById(R.id.tvPhone);
        if(dataResto!=null) {
            tvAddress.setText(dataResto.address);
            tvPhone.setText(dataResto.phone);
        }
    }


    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.mipmap.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            View mCustomView = LayoutInflater.from(this).inflate(R.layout.toolbar_header, null);
            android.support.v7.app.ActionBar.LayoutParams params = new android.support.v7.app.ActionBar.LayoutParams(
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT,
                    android.support.v7.app.ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);

            ImageView imgLogo=(ImageView)mCustomView.findViewById(R.id.imgLogo);
            TextView tvTittle=(TextView)mCustomView.findViewById(R.id.tvTittle);
            imgLogo.setVisibility(View.GONE);
            tvTittle.setVisibility(View.VISIBLE);
            tvTittle.setText(dataResto.name);

            actionBar.setCustomView(mCustomView, params);
            actionBar.setDisplayShowCustomEnabled(true);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        setMap();

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

    private void setMap(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions
                return false;
            }
        });

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(MONAS, 15));
        map.animateCamera(CameraUpdateFactory.zoomIn());
        map.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(MY_LOCATION)        // Sets the center of the map to Mountain View
                .zoom(13)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        int markerType = 0;
        markerType=R.drawable.icon_pin_center;
        Marker marker = map.addMarker(new MarkerOptions()
                .position(MY_LOCATION)
                .icon(BitmapDescriptorFactory.fromResource(markerType))
        );
        marker.showInfoWindow();


        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                TEMP_LOCATION = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
            }
        });
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
