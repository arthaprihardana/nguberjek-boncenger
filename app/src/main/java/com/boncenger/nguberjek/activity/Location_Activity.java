package com.boncenger.nguberjek.activity;

import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.API;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.ErrorCallback;
import com.boncenger.nguberjek.app.ResponseCallback;
import com.boncenger.nguberjek.helper.ConvertRupiah;
import com.boncenger.nguberjek.helper.GPSTracker;
import com.boncenger.nguberjek.model.AddressModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by adikurniawan on 11/19/17.
 */

public class Location_Activity extends AppCompatActivity implements OnMapReadyCallback , DirectionCallback {

    private GoogleMap map;
    private UiSettings mUiSettings;
    private LatLng MY_LOCATION = new LatLng(0, 0);
    private LatLng TEMP_LOCATION = new LatLng(0, 0);
    private LatLng RESTO_LOCATION = new LatLng(0,0);
    private LatLng MONAS = new LatLng(-6.1756484, 106.8227754);
    private GPSTracker gps;

    private String tag=Location_Activity.class.getName();

    private ProgressDialog progressDialog;


    private String distance="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_put_location);


        Bundle b = getIntent().getExtras();
        try {
            MY_LOCATION = new LatLng(b.getDouble("lat"), b.getDouble("lng"));
            RESTO_LOCATION = new LatLng(b.getDouble("lat_resto"), b.getDouble("lng_resto"));
        } catch (Throwable throwable) {
        }



        setContent();
        setNavigation();
    }

    private void setContent() {
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.framentMap);
            mapFragment.getMapAsync(this);
        } catch (Throwable tr) {
        }
    }

    private void setNavigation() {
        findViewById(R.id.btn_simpan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MY_LOCATION=TEMP_LOCATION;
                getGeoLocation();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (isMapPermissionGranted()) setMap();

        mUiSettings = map.getUiSettings();
        mUiSettings.setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

    private String TAG = "PERMISSION";

    public boolean isMapPermissionGranted(){
        if (Build.VERSION.SDK_INT >= 23) {
            int hasWriteContactsPermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            setMap();
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    private void setMap(){
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        if(MY_LOCATION.latitude==0) getCurrentLocation();
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions
                return false;
            }
        });

        //MY_LOCATION=new LatLng(-6.198029,106.782613);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(MONAS, 15));
        map.animateCamera(CameraUpdateFactory.zoomIn());
        map.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(MY_LOCATION)        // Sets the center of the map to Mountain View
                .zoom(13)                   // Sets the zoom
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                TEMP_LOCATION = new LatLng(cameraPosition.target.latitude, cameraPosition.target.longitude);
            }
        });
    }

    public void getCurrentLocation(){
        gps = new GPSTracker(this);
        if (gps.canGetLocation()) { // gps enabled
            MY_LOCATION = new LatLng(gps.getLatitude(), gps.getLongitude());
            // \n is for new line
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
            MY_LOCATION = MONAS;
            //gps.stopUsingGPS();
        }
    }

    private void showProgresDialog(){
        try {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Silahkan tunggu...");
            progressDialog.show();
        }catch (Throwable throwable){}
    }

    private void hideProgresDialog(){
        try{
            progressDialog.dismiss();
        }catch (Throwable throwable){}
    }

    public void getGeoLocation(){
        showProgresDialog();
        API.getGeoLocation(this,TEMP_LOCATION.latitude, TEMP_LOCATION.longitude, new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        hideProgresDialog();
                        try {
                            String formatted_address="";
                            JSONObject res=new JSONObject(response);
                            JSONArray jsonArray=res.getJSONArray("results");
                            if(jsonArray.length()>0){
                                JSONObject data=jsonArray.getJSONObject(0);
                                formatted_address=data.getString("formatted_address");
                            }
                            String[] address_split = formatted_address.split(",");
                            String address_name = address_split[0];
                            String address="";
                            for(int i=0;i<address_split.length;i++){
                                if(i!=0){
                                    address+=address_split[i];
                                }
                            }

                            AppConstants.addressModel.my_address_name= address_name+"\nalamat : \n"+address;
                            if (formatted_address.toLowerCase().startsWith("latitude:")) {
                                Toast.makeText(getApplicationContext(),"Maaf koneksi anda terputus, silahkan coba lagi",Toast.LENGTH_SHORT);
                            }

                            requestDirection();


                        } catch (JSONException e) {
                            Log.e("response", e.toString());
                        }
                        return httpCode;
                    }
                },
                new ErrorCallback() {
                    @Override
                    public void doAction() {
                        try {
                            hideProgresDialog();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void requestDirection() {
        Log.d(tag,"resto " +RESTO_LOCATION.toString());
        Log.d(tag,"my location " +MY_LOCATION.toString());
        GoogleDirection.withServerKey(API.API_KEY_GOOGLE)
                .from(RESTO_LOCATION)
                .to(MY_LOCATION)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
    }

    /**
     *
     * @param direction
     * @param rawBody
     */
    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        if (direction.isOK()) {

            // get distance  and  duration//
            String distance_text = direction.getRouteList().get(0).getLegList().get(0).getDistance().getText();
            String distance_value = direction.getRouteList().get(0).getLegList().get(0).getDistance().getValue();

            String duration = direction.getRouteList().get(0).getLegList().get(0).getDuration().getText();

            double distance_=0;
            try {
                distance_=Double.parseDouble(distance_value);
                distance_= distance_/1000;
            } catch (Throwable th) {
            }

            distance=distance_+"";
            getFare(distance_+"", distance_text, duration);

            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
            map.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.RED));

            //map.moveCamera(CameraUpdateFactory.newLatLngZoom(CURRENT_lOCATION, 13));
            //map.animateCamera(CameraUpdateFactory.zoomOut(), 2000, null);
        } else {
            Toast.makeText(getApplicationContext(), "direction gagal, silahkan coba lagi", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.d("direction", t.getMessage());
        //Snackbar.make(btnRequestDirection, t.getMessage(), Snackbar.LENGTH_SHORT).show();
    }


    public void getFare(final String distancee_value,final String distancee_text, final String durationn){
        showProgresDialog();
        API.getFare(this,distancee_value, new ResponseCallback() {
                    @Override
                    public int doAction(String response, int httpCode) {
                        hideProgresDialog();
                        try {
                            JSONObject responseObj=new JSONObject(response);
                            String status = responseObj.getString("s");
                            String message = responseObj.getString("msg");
                            if (status.equals("1")) {
                                String pricee = responseObj.getString("dt");
                            //  String pricee = response.getString("price");

                                MY_LOCATION = TEMP_LOCATION;
                                AppConstants.addressModel.price = pricee;
                                AppConstants.addressModel.distance = distancee_value;
                                AppConstants.addressModel.distanceLabel = distancee_text;
                                AppConstants.addressModel.duration = durationn;
                                AppConstants.addressModel.my_lat = TEMP_LOCATION.latitude;
                                AppConstants.addressModel.my_lng = TEMP_LOCATION.longitude;

                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }


                        } catch (JSONException e) {
                            Log.e("response", e.toString());
                        }
                        return httpCode;
                    }
                },
                new ErrorCallback() {
                    @Override
                    public void doAction() {
                        try {
                            hideProgresDialog();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


}


