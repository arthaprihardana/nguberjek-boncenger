package com.boncenger.nguberjek.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.helper.SessionManager;

/**
 * Created by sragen on 8/27/2016.
 */
public class SplashScreen   extends Activity {

    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        session=new SessionManager(this);


       /* ImageView imageView=(ImageView)findViewById(R.id.imageView);
        Glide.with(this)
                .load(R.drawable.splashnguber2)
                .asGif()
                .crossFade()
                .into(imageView);*/

        WebView webView=(WebView)findViewById(R.id.webView);
        webView.loadUrl("file:///android_asset/splash_screen.html");



        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(5000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    if (session.isLoggedIn()) {
                        Intent launchactivity = new Intent(SplashScreen.this, HomeAcitivity.class);
                        startActivity(launchactivity);
                    } else {
                        Intent launchactivity = new Intent(SplashScreen.this, IntroPageActivity.class);
                        startActivity(launchactivity);
                    }
                    finish();
                }
            }
        };
        timerThread.start();
    }
}
