package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.boncenger.nguberjek.R;

/**
 * Created by sragen on 8/12/2016.
 */
public class MenuAdapter  extends BaseAdapter {
    Context context;
    LayoutInflater inflater;
    int[] images={
            R.mipmap.histori,
            R.mipmap.favorit,
            R.mipmap.bantuan,
            R.mipmap.ic_booking
    };
    String[] menus={
            "Riwayat",
            "Favorit",
            "Bantuan",
            "Booking"
    };


    public MenuAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return menus.length;
    }

    @Override
    public Object getItem(int location) {
        return menus[location];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_menu, null);


        TextView tvMenu = (TextView) convertView.findViewById(R.id.tvMenu);
        ImageView imageIcon =(ImageView)convertView.findViewById(R.id.imageIcon);

        tvMenu.setText(menus[position]);
        Glide
                .with(context)
                .load(images[position])
                .crossFade()
                .into(imageIcon);


        return convertView;

    }

}
