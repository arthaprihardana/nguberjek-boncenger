package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.RestoMenuActivity;
import com.boncenger.nguberjek.fragment.MenuFragment;
import com.boncenger.nguberjek.model.MenuModel;
import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by adikurniawan on 10/20/17.
 */

public class MenuRestoAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    Context context;
    ArrayList<MenuModel> rowList=new ArrayList<MenuModel>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    public MenuRestoAdapter(Context context, ArrayList<MenuModel> rowList){
        this.context=context;
        this.rowList=rowList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_resto, viewGroup, false);

            return new MenuRestoAdapter.ViewHolder(itemView);
        } else if (i == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_resto, viewGroup, false);

            return new MenuRestoAdapter.ViewHolderHeader(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView tvName, tvPrice;
        Button btnAdd;
        TextView tvMinus, tvQty, tvPlus;
        LinearLayout containerQty;


        ViewHolder(View itemView) {
            super(itemView);
            imageView=(ImageView)itemView.findViewById(R.id.imgMenu);
            tvName=(TextView)itemView.findViewById(R.id.tvName);
            tvPrice=(TextView)itemView.findViewById(R.id.tvPrice);
            btnAdd=(Button)itemView.findViewById(R.id.btnAdd);
            tvQty=(TextView)itemView.findViewById(R.id.tvQty);
            tvMinus=(TextView)itemView.findViewById(R.id.tvMinus);
            tvPlus=(TextView)itemView.findViewById(R.id.tvPlus);
            containerQty=(LinearLayout)itemView.findViewById(R.id.containerQty);


        }
    }

    static class ViewHolderHeader extends RecyclerView.ViewHolder {

        ViewHolderHeader(View itemView) {
            super(itemView);

        }
    }

    @Override
    public int getItemCount() {
        return rowList.size();
    }

    @Override
    public int getItemViewType(int position){
        if (position==0)
            return TYPE_ITEM;

        return TYPE_ITEM;
    }



    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder variableViewHolder,final int i) {

        if (variableViewHolder instanceof MenuRestoAdapter.ViewHolder) {
            final MenuRestoAdapter.ViewHolder holder = (MenuRestoAdapter.ViewHolder) variableViewHolder;
            final MenuModel data=rowList.get(i);

            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

            Glide.with(context)
                    .load(data.image)
                    .placeholder(R.drawable.img_placeholder)
                    .into(holder.imageView);

            holder.tvName.setText(data.name);
            holder.tvPrice.setText(""+formatRupiah.format(data.price));

            if (data.qty > 0) {
                holder.containerQty.setVisibility(View.VISIBLE);
                holder.btnAdd.setVisibility(View.GONE);
            }else{
                holder.containerQty.setVisibility(View.GONE);
                holder.btnAdd.setVisibility(View.VISIBLE);
            }

            holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(data.qty>0) {
                        data.qty = data.qty - 1;
                    }
                    notifyDataSetChanged();
                    ((RestoMenuActivity)context).updateCart(rowList);
                }
            });
            holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.qty=data.qty+1;
                    notifyDataSetChanged();
                    ((RestoMenuActivity)context).updateCart(rowList);
                }
            });
            holder.tvQty.setText(data.qty+"");

            holder.btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.qty=data.qty+1;
                    notifyDataSetChanged();
                    ((RestoMenuActivity)context).updateCart(rowList);
                }
            });

        } else if (variableViewHolder instanceof MenuRestoAdapter.ViewHolderHeader) {
            MenuRestoAdapter.ViewHolderHeader holder = (MenuRestoAdapter.ViewHolderHeader) variableViewHolder;


        }

    }





}