package com.boncenger.nguberjek.adapter;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.media.Image;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.model.CategoryModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import static com.boncenger.nguberjek.app.AppController.TAG;

/**
 * Created by adikurniawan on 10/16/17.
 */

public class KategoriFoodAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    Context context;
    ArrayList<CategoryModel> rowList=new ArrayList<CategoryModel>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    public KategoriFoodAdapter(Context context, ArrayList<CategoryModel> rowList){
        this.context=context;
        this.rowList=rowList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_kategori_food, viewGroup, false);

            return new KategoriFoodAdapter.ViewHolder(itemView);
        } else if (i == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.header_category_food, viewGroup, false);

            return new KategoriFoodAdapter.ViewHolderHeader(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        ImageView imageView;
        TextView tvName;


        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            imageView=(ImageView)itemView.findViewById(R.id.imageView);
            tvName=(TextView)itemView.findViewById(R.id.tvName);


        }
    }

    static class ViewHolderHeader extends RecyclerView.ViewHolder {
        ImageView imgBanner, imgPromo, imgNear;

        ViewHolderHeader(View itemView) {
            super(itemView);

            imgBanner=(ImageView)itemView.findViewById(R.id.imgBanner);
            imgPromo=(ImageView)itemView.findViewById(R.id.imgPromo);
            imgNear=(ImageView)itemView.findViewById(R.id.imgNear);
        }
    }

    @Override
    public int getItemCount() {
        return rowList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
            return TYPE_ITEM;

        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder variableViewHolder,final int i) {

        if (variableViewHolder instanceof KategoriFoodAdapter.ViewHolder) {
            final KategoriFoodAdapter.ViewHolder holder = (KategoriFoodAdapter.ViewHolder) variableViewHolder;
            CategoryModel data=rowList.get(i);
            Glide.with(context)
                    .load(data.image)
                    .into(holder.imageView);
            holder.tvName.setText(data.name);

        } else if (variableViewHolder instanceof KategoriFoodAdapter.ViewHolderHeader) {
            KategoriFoodAdapter.ViewHolderHeader holder = (KategoriFoodAdapter.ViewHolderHeader) variableViewHolder;

            Glide.with(context)
                    .load(R.drawable.banner_nguberfood)
                    .into(holder.imgBanner);
            Glide.with(context)
                    .load(R.drawable.promotion)
                    .into(holder.imgPromo);
            Glide.with(context)
                    .load(R.drawable.near_me)
                    .into(holder.imgNear);

        }

    }



}