package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.ConfirmOrderFoodActivity;
import com.boncenger.nguberjek.activity.RestoMenuActivity;
import com.boncenger.nguberjek.model.AddressModel;
import com.boncenger.nguberjek.model.MenuModel;
import com.boncenger.nguberjek.model.MenuModel;
import com.bumptech.glide.Glide;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by adikurniawan on 11/12/17.
 */

public class ConfirmCartAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    Context context;
    ArrayList<MenuModel> rowList=new ArrayList<MenuModel>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_FOOTER = 2;
    AddressModel addressModel;


    public ConfirmCartAdapter(Context context, ArrayList<MenuModel> rowList, AddressModel addressModel){
        this.context=context;
        this.rowList=rowList;
        this.addressModel=addressModel;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart, viewGroup, false);

            return new ConfirmCartAdapter.ViewHolder(itemView);
        } else if (i == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart_header, viewGroup, false);

            return new ConfirmCartAdapter.ViewHolderHeader(itemView);
        }else if(i==TYPE_FOOTER){
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_menu_cart_footer, viewGroup, false);

            return new ConfirmCartAdapter.ViewHolderFooter(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvMenuName;
        TextView tvMenuPrice;
        TextView tvQty,tvMinus, tvPlus;
        TextView tvNote;
        EditText etNote;


        ViewHolder(View itemView) {
            super(itemView);
            tvMenuName=(TextView)itemView.findViewById(R.id.tvMenuName);
            tvMenuPrice=(TextView)itemView.findViewById(R.id.tvMenuPrice);
            tvQty=(TextView)itemView.findViewById(R.id.tvQty);
            tvMinus=(TextView)itemView.findViewById(R.id.tvMinus);
            tvPlus=(TextView)itemView.findViewById(R.id.tvPlus);
            tvNote=(TextView)itemView.findViewById(R.id.tvAddNote);
            etNote=(EditText)itemView.findViewById(R.id.etNote);

        }
    }

    static class ViewHolderHeader extends RecyclerView.ViewHolder {

        ViewHolderHeader(View itemView) {
            super(itemView);

        }
    }

    static class ViewHolderFooter extends RecyclerView.ViewHolder {
        TextView tvAddMore;
        TextView btnLocation;
        TextView tvAddNote;
        EditText etNote;
        TextView tvAddress, tvAddressResto;
        ImageView imgMap, imgMapResto;

        // payment details //
        TextView tvLabelDelivery;
        TextView tvPriceFood, tvPriceDelivery, tvPriceTotal;

        ViewHolderFooter(View itemView) {
            super(itemView);

            tvAddMore=(TextView)itemView.findViewById(R.id.btnAddMore);
            btnLocation=(TextView) itemView.findViewById(R.id.tvChange);
            tvAddNote=(TextView)itemView.findViewById(R.id.tvAddNote);
            etNote=(EditText) itemView.findViewById(R.id.etNote);
            tvAddress=(TextView)itemView.findViewById(R.id.tvAddress);
            imgMap=(ImageView)itemView.findViewById(R.id.img_map);
            tvAddressResto=(TextView)itemView.findViewById(R.id.tvAddressResto);
            imgMapResto=(ImageView)itemView.findViewById(R.id.img_map_resto);


            tvLabelDelivery=(TextView)itemView.findViewById(R.id.tvLabelDelivery);
            tvPriceFood=(TextView)itemView.findViewById(R.id.tvPriceFood);
            tvPriceDelivery=(TextView)itemView.findViewById(R.id.tvPriceDelivery);
            tvPriceTotal=(TextView)itemView.findViewById(R.id.tvTotalPrice);


        }
    }

    @Override
    public int getItemCount() {
        return rowList.size()+2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
            return TYPE_HEADER;
        else if(position==rowList.size()+1)
            return TYPE_FOOTER;
        else
            return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder variableViewHolder,final int i) {

        if (variableViewHolder instanceof ConfirmCartAdapter.ViewHolder) {
            final ConfirmCartAdapter.ViewHolder holder = (ConfirmCartAdapter.ViewHolder) variableViewHolder;
            final MenuModel data=rowList.get(i-1);

            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

            holder.tvMenuName.setText(data.name);
            holder.tvQty.setText(data.qty+"");
            int subTotal=data.qty*data.price;
            holder.tvMenuPrice.setText(""+formatRupiah.format(subTotal));

            if (holder.etNote != null)
                holder.etNote.setText(data.note);
            holder.etNote.addTextChangedListener(new EditTextWatcher(data));


            holder.tvMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(data.qty>0) {
                        data.qty = data.qty - 1;
                    }
                    notifyDataSetChanged();
                    ((ConfirmOrderFoodActivity)context).updateCart(rowList);
                }
            });
            holder.tvPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    data.qty=data.qty+1;
                    notifyDataSetChanged();
                    ((ConfirmOrderFoodActivity)context).updateCart(rowList);
                }
            });

            if(data.note.length()>0){
                holder.tvNote.setVisibility(View.GONE);
                holder.etNote.setVisibility(View.VISIBLE);
            }else{
                holder.tvNote.setVisibility(View.VISIBLE);
                holder.etNote.setVisibility(View.GONE);
            }

            holder.tvNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tvNote.setVisibility(View.GONE);
                    holder.etNote.setVisibility(View.VISIBLE);
                    holder.etNote.requestFocus();
                }
            });




        } else if (variableViewHolder instanceof ConfirmCartAdapter.ViewHolderHeader) {
            ConfirmCartAdapter.ViewHolderHeader holder = (ConfirmCartAdapter.ViewHolderHeader) variableViewHolder;




        } else if(variableViewHolder instanceof  ConfirmCartAdapter.ViewHolderFooter){
            final ConfirmCartAdapter.ViewHolderFooter holder =(ConfirmCartAdapter.ViewHolderFooter) variableViewHolder;

            holder.tvAddMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ConfirmOrderFoodActivity)context).finish();
                }
            });
            holder.btnLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ConfirmOrderFoodActivity)context).goLocation();
                }
            });

            holder.etNote.setText(addressModel.note);
            if(addressModel.note.length()>0){
                holder.tvAddNote.setVisibility(View.GONE);
                holder.etNote.setVisibility(View.VISIBLE);
            }else{
                holder.tvAddNote.setVisibility(View.GONE);
                holder.etNote.setVisibility(View.GONE);
            }

            holder.tvAddNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.tvAddNote.setVisibility(View.GONE);
                    holder.etNote.setVisibility(View.VISIBLE);
                    holder.etNote.requestFocus();
                }
            });

            if(addressModel.my_address_name.equals(""))
                holder.tvAddress.setText("Belum ada alamat");
            else
                holder.tvAddress.setText(addressModel.my_address_name);

            if(addressModel.resto_address_name.equals(""))
                holder.tvAddressResto.setText("Belum ada alamat");
            else
                holder.tvAddressResto.setText(addressModel.resto_address_name);


            if (holder.etNote != null)
                holder.etNote.setText(addressModel.note);
            holder.etNote.addTextChangedListener(new EditTextWatcherAddress(addressModel));


            String image_url = "http://maps.google.com/maps/api/staticmap?center=" + addressModel.my_lat + "," + addressModel.my_lng + "&zoom=15&size=700x400&sensor=true";
            Glide.with(context)
                    .load(image_url)
                    .into(holder.imgMap);

            String image_url_resto = "http://maps.google.com/maps/api/staticmap?center=" + addressModel.resto_lat + "," + addressModel.resto_lng + "&zoom=15&size=700x400&sensor=true";
            Glide.with(context)
                    .load(image_url_resto)
                    .into(holder.imgMapResto);


            /// payment details ///

            Locale localeID = new Locale("in", "ID");
            NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

            int total=0;
            int price=0;
            int priceDelivery=0;
            for(int x=0;x<rowList.size();x++){
                price=price+(rowList.get(x).qty * rowList.get(x).price);
            }

            try{
                priceDelivery= Integer.parseInt(addressModel.price);
            }catch (Throwable throwable){}
            total=price+priceDelivery;


            holder.tvPriceFood.setText(formatRupiah.format(price));
            holder.tvLabelDelivery.setText("Delivery ("+addressModel.distanceLabel+")");
            if(!addressModel.price.equals(""))
                holder.tvPriceDelivery.setText(formatRupiah.format(priceDelivery));
            holder.tvPriceTotal.setText(formatRupiah.format(total));

        }

    }


    public class EditTextWatcher implements TextWatcher {

        private MenuModel data;

        public EditTextWatcher(MenuModel data) {
            this.data = data;
        }

        @Override
        public void afterTextChanged(Editable s) {
            data.note=s.toString();
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

    }

    public class EditTextWatcherAddress implements TextWatcher {

        private AddressModel data;

        public EditTextWatcherAddress(AddressModel data) {
            this.data = data;
        }

        @Override
        public void afterTextChanged(Editable s) {
            data.note=s.toString();
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

        }

    }



}