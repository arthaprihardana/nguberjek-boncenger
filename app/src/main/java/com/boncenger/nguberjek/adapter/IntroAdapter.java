package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boncenger.nguberjek.R;

/**
 * Created by sragen on 8/9/2016.
 */
public class IntroAdapter extends PagerAdapter {
    Context mContext;
    LayoutInflater mLayoutInflater;
    private String[] items;


    public IntroAdapter(Context context,String[] items ) {
        this.mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items=items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_intro, container, false);

        TextView tvIntro=(TextView)itemView.findViewById(R.id.tvIntro);
        tvIntro.setText(Html.fromHtml(items[position]));

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}
