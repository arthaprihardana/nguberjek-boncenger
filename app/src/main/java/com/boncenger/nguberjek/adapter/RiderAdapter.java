package com.boncenger.nguberjek.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.bumptech.glide.Glide;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.LogManager;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.RiderModel2;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sragen on 8/19/2016.
 */
public class RiderAdapter   extends RecyclerView.Adapter <RiderAdapter.ViewHolder> {

    Activity context;
    ArrayList<RiderModel2> riderList=new ArrayList<RiderModel2>();

    LogManager log=new LogManager();
    SessionManager session;
    HashMap<String,String> user;

    public RiderAdapter(Activity context, ArrayList <RiderModel2> riderList){
        this.context=context;
        this.riderList=riderList;

        session=new SessionManager(context);
        user=session.getUserDetails();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_rider, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView tvName;
        TextView tvNopol;
        TextView tvKategori;
        ImageView imgRider;
        ImageView imgHapus;
        TextView tvCall;


        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);

            tvName=(TextView)itemView.findViewById(R.id.tvName);
            tvNopol=(TextView)itemView.findViewById(R.id.tvNopol);
            tvKategori=(TextView)itemView.findViewById(R.id.tvKategori);
            tvCall=(TextView)itemView.findViewById(R.id.tvCall);
            imgRider=(ImageView)itemView.findViewById(R.id.imgTo);
            imgHapus=(ImageView)itemView.findViewById(R.id.imgRemove);
        }
    }

    @Override
    public int getItemCount() {
        return riderList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder variableViewHolder, final int i) {

        final RiderModel2 model=riderList.get(i);
        variableViewHolder.tvName.setText(model.rider_name);
        variableViewHolder.tvNopol.setText(model.rider_nopol);
        variableViewHolder.tvKategori.setText(model.rider_kategori);

        Glide.with(context)
                .load(model.rider_photo)
                .error(R.mipmap.icon_profil)
                .placeholder(R.mipmap.icon_profil)
                .crossFade()
                .bitmapTransform(new CropCircleTransformation(context))
                .into(variableViewHolder.imgRider);

        variableViewHolder.imgHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusFavorit(model.rider_id,i);
            }
        });

        variableViewHolder.tvCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(i);
            }
        });

    }


    public void hapusFavorit(final String id,final int pos){

        final String url= AppConstants.APIRiderFavorit
                +"?boncengerId="+user.get(SessionManager.KEY_MEMBER_ID)
                +"&riderId="+id;
        log.logD(url);
        String tag_json_obj="delRiderFav";
        final ProgressDialog dialog= ProgressDialog.show(context, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        riderList.remove(pos);
                        dialog_message_listener(message);
                    }else{
                        dialog_message(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();


                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void call(int position){
        String number_phone = riderList.get(position).rider_phone;
        number_phone = number_phone.replace(" ", "");
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number_phone + ""));
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        context.startActivity(callIntent);
    }

    public void dialog_message_listener(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                notifyDataSetChanged();
            }
        });
        builder.show();
    }

    public void dialog_message(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }


}
