package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.RestoMenuActivity;
import com.boncenger.nguberjek.model.RestoModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by adikurniawan on 10/19/17.
 */

public class RestoAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder> {

    Context context;
    ArrayList<RestoModel> rowList=new ArrayList<RestoModel>();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;


    public RestoAdapter(Context context, ArrayList<RestoModel> rowList){
        this.context=context;
        this.rowList=rowList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        if (i == TYPE_ITEM) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_resto, viewGroup, false);

            return new RestoAdapter.ViewHolder(itemView);
        } else if (i == TYPE_HEADER) {
            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.item_resto, viewGroup, false);

            return new RestoAdapter.ViewHolderHeader(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + i + " + make sure your using types correctly");
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        ImageView imageView;
        TextView tvName, tvAddress, tvOpeningHours;


        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.card_view);
            imageView=(ImageView)itemView.findViewById(R.id.imgResto);
            tvName=(TextView)itemView.findViewById(R.id.tv_name);
            tvAddress=(TextView)itemView.findViewById(R.id.tv_address);
            tvOpeningHours=(TextView)itemView.findViewById(R.id.tv_opening_hours);


        }
    }

    static class ViewHolderHeader extends RecyclerView.ViewHolder {

        ViewHolderHeader(View itemView) {
            super(itemView);

        }
    }

    @Override
    public int getItemCount() {
        return rowList.size();
    }

    @Override
    public int getItemViewType(int position){
        if (position==0)
            return TYPE_ITEM;

        return TYPE_ITEM;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder variableViewHolder,final int i) {

        if (variableViewHolder instanceof RestoAdapter.ViewHolder) {
            final RestoAdapter.ViewHolder holder = (RestoAdapter.ViewHolder) variableViewHolder;
            final RestoModel data=rowList.get(i);

            Glide.with(context)
                    .load(data.image)
                    .placeholder(R.drawable.img_placeholder)
                    .into(holder.imageView);

            holder.tvName.setText(data.name);
            holder.tvAddress.setText(data.address);
            holder.tvOpeningHours.setText(data.openingHours);

            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b=new Bundle();
                    b.putSerializable("data_resto",data);
                    Intent intent=new Intent(context, RestoMenuActivity.class);
                    intent.putExtras(b);
                    context.startActivity(intent);
                }
            });

        } else if (variableViewHolder instanceof RestoAdapter.ViewHolderHeader) {
            RestoAdapter.ViewHolderHeader holder = (RestoAdapter.ViewHolderHeader) variableViewHolder;


        }

    }



}