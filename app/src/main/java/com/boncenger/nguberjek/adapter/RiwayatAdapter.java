package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.RiwayatActivity;
import com.boncenger.nguberjek.model.HistoryModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sragen on 8/19/2016.
 */
public class RiwayatAdapter   extends RecyclerView.Adapter <RiwayatAdapter.ViewHolder> {

    Context context;
    ArrayList<HistoryModel> historyList=new ArrayList<HistoryModel>();
    public RiwayatAdapter(Context context,ArrayList<HistoryModel>historyList){
        this.context=context;
        this.historyList=historyList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_riwayat, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView tvDate;
        TextView tvLokasiAwal;
        TextView tvLokasiTujuan;
        TextView tvKategori;
        TextView tvJadikanFav;

        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            tvDate =(TextView)itemView.findViewById(R.id.tvDate);
            tvLokasiAwal =(TextView)itemView.findViewById(R.id.tvLokasiAwal);
            tvLokasiTujuan =(TextView)itemView.findViewById(R.id.tvLokasiTujuan);
            tvKategori =(TextView)itemView.findViewById(R.id.tvKategori);
            tvJadikanFav=(TextView)itemView.findViewById(R.id.tvJadikanFav);
        }
    }

    @Override
    public int getItemCount() {
        return historyList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder variableViewHolder, int i) {

        final HistoryModel data=historyList.get(i);
        variableViewHolder.tvDate.setText(customConvertDate(data.order_time));
        variableViewHolder.tvLokasiAwal.setText(data.lokasi_awal);
        variableViewHolder.tvLokasiTujuan.setText(data.lokasi_tujuan);
        variableViewHolder.tvKategori.setText(data.order_kategori);


        variableViewHolder.tvJadikanFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((RiwayatActivity)context).jadikanFav(data.order_id);
            }
        });

    }

    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy, hh:mm");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }


}
