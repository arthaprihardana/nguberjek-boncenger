package com.boncenger.nguberjek.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.ChatModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by adikurniawan on 10/8/16.
 */
public class ChatAdapter extends RecyclerView.Adapter <ChatAdapter.ViewHolder> {

    Context context;
    ArrayList<ChatModel> chatList=new ArrayList<ChatModel>();

    public ChatAdapter(Context context, ArrayList<ChatModel>chatList){
        this.context=context;
        this.chatList=chatList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_chat_list, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName,tvMessage, tvDate;
        RelativeLayout layout;
        LinearLayout parent_layout;


        ViewHolder(View itemView) {
            super(itemView);
            tvName =(TextView)itemView.findViewById(R.id.tvName);
            tvMessage =(TextView)itemView.findViewById(R.id.tvMessage);
            tvDate =(TextView)itemView.findViewById(R.id.tvDate);
            parent_layout = (LinearLayout) itemView.findViewById(R.id.lnParent);
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder variableViewHolder, int i) {

        final ChatModel data=chatList.get(i);

        variableViewHolder.tvMessage.setText(data.message);
        //variableViewHolder.tvDate.setText(data.date);
        variableViewHolder.tvDate.setText(customConvertDate(data.date));

        if(data.isBoncenger){
            //variableViewHolder.layout.setBackgroundResource(R.drawable.bubble2);
            variableViewHolder.tvName.setText("Boncenger: ");
            variableViewHolder.parent_layout.setGravity(Gravity.RIGHT);
        }else {
            //variableViewHolder.layout.setBackgroundResource(R.drawable.bubble1);
            variableViewHolder.tvName.setText("Rider: ");
            variableViewHolder.parent_layout.setGravity(Gravity.LEFT);
        }

    }

    public String customConvertDate(String currentDate){
        String date = currentDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }


}