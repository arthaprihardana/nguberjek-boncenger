package com.boncenger.nguberjek.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.boncenger.nguberjek.R;
import com.boncenger.nguberjek.activity.HomeAcitivity;
import com.boncenger.nguberjek.app.AppConstants;
import com.boncenger.nguberjek.app.AppController;
import com.boncenger.nguberjek.app.CustomRequest;
import com.boncenger.nguberjek.helper.ActivityManager;
import com.boncenger.nguberjek.helper.LogManager;
import com.boncenger.nguberjek.helper.SessionManager;
import com.boncenger.nguberjek.model.RuteModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sragen on 8/19/2016.
 */
public class PerjalananAdapter  extends RecyclerView.Adapter <PerjalananAdapter.ViewHolder> {

    Context context;
    ArrayList<RuteModel> ruteList=new ArrayList<RuteModel>();

    LogManager log=new LogManager();
    SessionManager session;
    HashMap<String,String> user;

    public PerjalananAdapter(Context context, ArrayList<RuteModel> ruteList){
        this.context=context;
        this.ruteList=ruteList;
        session=new SessionManager(context);
        user=session.getUserDetails();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_perjalanan, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView tvLokasiAwal;
        TextView tvLokasiTujuan;
        TextView tvJarak;
        TextView tvKategori;
        TextView tvPesan;
        ImageView imgRemove;


        ViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            tvJarak=(TextView)itemView.findViewById(R.id.tvJarak);
            tvKategori=(TextView)itemView.findViewById(R.id.tvKategori);
            tvLokasiAwal=(TextView)itemView.findViewById(R.id.tvLokasiAwal);
            tvLokasiTujuan=(TextView)itemView.findViewById(R.id.tvLokasiTujuan);
            tvPesan=(TextView)itemView.findViewById(R.id.tvPesanSekarang);
            imgRemove=(ImageView)itemView.findViewById(R.id.imgRemove);
        }
    }

    @Override
    public int getItemCount() {
        return ruteList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder variableViewHolder, final int i) {



        final RuteModel model=ruteList.get(i);
        variableViewHolder.tvLokasiAwal.setText(model.lokasiAwal);
        variableViewHolder.tvLokasiTujuan.setText(model.lokasiAkhir);
        variableViewHolder.tvKategori.setText(model.kategori);
        variableViewHolder.tvJarak.setText(model.jarak+" KM");


        variableViewHolder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hapusFavorit(model.orderId,i);
            }
        });
        variableViewHolder.tvPesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                Intent i=new Intent(context, HomeAcitivity.class);
                b.putSerializable("ruteModel",model);
                i.putExtras(b);
                context.startActivity(i);
                ActivityManager.getInstance().finishAll();
            }
        });
    }

    public void hapusFavorit(final String order_id,final int pos){

        final String url= AppConstants.APIRuteFavorit+"?orderId="+order_id;
        log.logD(url);
        String tag_json_obj="delRuteFav";
        final ProgressDialog dialog= ProgressDialog.show(context, "", "loading", true);
        dialog.setCancelable(true);

        // creating request obj
        CustomRequest jsonObjReq= new CustomRequest(Request.Method.DELETE, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    log.logD(response.toString());
                    String status=response.getString("s");
                    String message=response.getString("msg");

                    if(status.equals("1")){
                        ruteList.remove(pos);
                        dialog_message_listener(message);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context,
                            e.toString() ,
                            Toast.LENGTH_LONG).show();
                    dialog.dismiss();

                }
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error){
                VolleyLog.d(getClass().getSimpleName(), "Error: " + error.getMessage());
                dialog.dismiss();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("x-access-token",user.get(SessionManager.KEY_MEMBER_TOKEN) );
                log.logD(params.toString());
                return params;
            }

            @Override
            protected Map<String, String> getParams() {


                Map<String, String> params = new HashMap<String, String>();


                log.logD(params.toString());
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }

    public void dialog_message_listener(String message){
        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                notifyDataSetChanged();
            }
        });
        builder.show();
    }


}
