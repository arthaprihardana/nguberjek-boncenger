package com.boncenger.nguberjek;

import com.boncenger.nguberjek.model.SavedAddress;

import java.util.List;

/**
 * Created by adikurniawan on 24/02/18.
 */

interface SavedPlaceListener {
    void onSavedPlaceClick(List<SavedAddress> mResponse, int position);
}
